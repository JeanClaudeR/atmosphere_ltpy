# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 11:09:51 2021

@author: Max
"""
import torch
import torch.nn as nn

class Residual_Block(nn.Module): 
    def __init__(self, sizeConv):
        super(Residual_Block, self).__init__()

        self.conv1 = nn.Conv2d(in_channels=sizeConv, out_channels=sizeConv, kernel_size=3, stride=1, padding=1, bias=False)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = nn.Conv2d(in_channels=sizeConv, out_channels=sizeConv, kernel_size=3, stride=1, padding=1, bias=False)

    def forward(self, x): 
        identity_data = x
        output = self.relu(self.conv1(x))
        output = self.conv2(output)
        output *= 0.1
        output = torch.add(output,identity_data)
        return output 

class EDSR(nn.Module):
    def __init__(self, nc, nlayers, sizeConv, scale):
        super(EDSR, self).__init__()
  
        self.conv_input = nn.Conv2d(in_channels=nc, out_channels=sizeConv, kernel_size=3, stride=1, padding=1, bias=False)

        self.residual = self.make_layer(Residual_Block, nlayers, sizeConv)

        self.conv_mid = nn.Conv2d(in_channels=sizeConv, out_channels=sizeConv, kernel_size=3, stride=1, padding=1, bias=False)

        self.upscale = self.upscaleX(sizeConv, scale)

        self.conv_output = nn.Conv2d(in_channels=sizeConv, out_channels=nc, kernel_size=3, stride=1, padding=1, bias=False)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
                if m.bias is not None:
                    m.bias.data.zero_()
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                if m.bias is not None:
                    m.bias.data.zero_()

    def upscaleX(self, sizeConv, scale) :
        seq = []
        for _ in range(int(math.log(scale, 2))):
            seq.append(nn.Conv2d(in_channels=sizeConv, out_channels=sizeConv*(scale**2), kernel_size=3, stride=1, padding=1, bias=False))
            seq.append(nn.PixelShuffle(2))
        return nn.Sequential(*seq)
            
    def make_layer(self, block, num_of_layer, sizeConv):
        layers = []
        for _ in range(num_of_layer):
            layers.append(block(sizeConv))
        return nn.Sequential(*layers)

    def forward(self, x):
        out = self.conv_input(x)
        residual = out
        out = self.conv_mid(self.residual(out))
        out = torch.add(out,residual)
        out = self.upscale(out)
        out = self.conv_output(out)
        return out