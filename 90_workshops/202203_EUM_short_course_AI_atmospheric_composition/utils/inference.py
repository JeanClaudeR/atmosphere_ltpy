import os
import numpy as np
import shutil
import time

from fastai.data.all import *
from fastai.vision.all import *
from fastai.vision.core import *
from fastai.vision.data import *
import pdb

from utils.UGeop import GRaster
R=GRaster.Raster()

def standardization(array, mean, std) : # array / mean / standard deviation
        return (array-mean)/std # compute the standardized image
    
def normalization(array, minimum, maximum): # array to normalized / minimum and maximum to consider for the normalization (what range)
    return (array - minimum)/(maximum - minimum) # compute the normalized image

def compareList(l1, l2): # function to return boolean list to compare two lists
    return [i==j for i, j in zip(l1, l2)]

def SISR(img, resDST, learn, size, scale, inpMIN, inpMAX, tarMIN, tarMAX) :
    # Temporary folders for the pre processing
    tF = 'memTILES'
    if os.path.isdir(tF):
        shutil.rmtree(tF)
    oF = 'memRESULTS'
    if os.path.isdir(oF):
        shutil.rmtree(oF)
    if os.path.isfile('memREZ.tif'):
        os.remove('memREZ.tif')        
    if os.path.isfile('stdMEM.tif'):
        os.remove('stdMEM.tif')
    if os.path.isfile('mem.tif'):
        os.remove('mem.tif')
    
    start = time.time()
    print('Standardization ...')
    geoInfo = R.getGeoInfo(img)
    R.resizing(img, 'memREZ.tif', width = int(geoInfo['Width']*scale), height = int(geoInfo['Height']*scale),resampleAlg=1, dtype = 'Float32')
    data = R.asArray('memREZ.tif') # open the image as an array using gdal
    norm = normalization(data, inpMIN, inpMAX)
    norm = np.where(norm>1, 1, norm)
    norm = np.where(norm<0, 0, norm)
    R.save_tiff('stdMEM.tif', norm, 'memREZ.tif', 'memREZ.tif', dtype='Float32') 
    

    # the image should be normalized exactly in the same way than the previous input
    # if not the model wont be able to extract any information
    end = time.time()
    print((end - start)/60 ,'min')
    start = time.time()
    
    print('Tiling ...')
    # Tiling is necessary to keep the same dimension than the input
    os.mkdir(tF)
    dst = tF + '/t_'
    R.tiling('stdMEM.tif', dst, size, int(size/2), dtype='Float32')
    end = time.time()
    print((end - start)/60 ,'min')   
    start = time.time()
    
    print('Prediction ...')    
    # Prediction
    os.mkdir(oF)
    tiles = [os.path.join(tF, x) for x in sorted(os.listdir(tF)) if x.endswith('.tif')]
    # the prediction is made on each tile
    
    for t in tiles :
        inp, res, _, _ = learn.predict(Path(t), with_input=True)
        # from the result we get the input image, and the decoded result
        org = np.array(inp).reshape(inp.shape[1], inp.shape[1])    
        out = np.array(res).reshape(res.shape[1], res.shape[1])
        outGeoInfo = R.getGeoInfo(t)
        # create a fake output of same size to extract geometry information
        #R.resizing(t, 'mem.tif', width = int(geoInfo['Width']*scale), height = int(geoInfo['Height']*scale), dtype = 'Float32')
        #outGeoInfo = R.getGeoInfo('mem.tif')
        outgeom = outGeoInfo['Geometry']
        outprj = outGeoInfo['Projection']
        pad = np.zeros_like(out) # create a copy of the result to mask border effects
        pad[5:-5,5:-5] = 1 # manage the rest of the image
        outpad = np.where(pad==1, out, pad) # apply the mask over the image
        SRtile = oF + '/SR_{}'.format(t.split('/')[-1:][0]) 
        R.save_tiff(SRtile, out, outprj, outgeom, dtype='Float32') # saving the result tile
        SRpadtile = oF + '/pad_{}'.format(t.split('/')[-1:][0]) 
        R.save_tiff(SRpadtile, outpad, outprj, outgeom, dtype='Float32') # saving the result tile with pad

    end = time.time()
    print((end - start)/60 ,'min')
    start = time.time()
    
    print('Reconstruction ...')    
    # Image reconstruction
    lres = [os.path.join(oF, x) for x in sorted(os.listdir(oF)) if 'SR' in x] # from the tiles we obtained as result
    R.merge(lres, resDST,rsmple_alg=3, srcNodata=0)# we merge all together to retrieve the entire image given as input and consider the mask as non data to get only best measurements
    lpad = [os.path.join(oF, x) for x in sorted(os.listdir(oF)) if 'pad' in x] # we retrieve the padded images
    R.merge(lpad, 'mem.tif',rsmple_alg=3, srcNodata=0) # merged image with the pad has border of nan value
    paddedRes = R.asArray('mem.tif')
    finalOUTPUT = R.asArray(resDST)
    finalOUTPUT = np.where(paddedRes<=0, finalOUTPUT, paddedRes) # we replace the nan value border with real values of the output
    # and we replace values inside with the one extracted from the padded images, like that we can avoid a grid effect in the reconstruction
    
    # the result is set up between 0 and 1 and we would like to get back the original measurment
    # we then process a de-normalization : to do this we have to go the other way around
    denorm = finalOUTPUT * (tarMAX - tarMIN) + tarMIN
    denorm = np.where(denorm<0, 0, denorm)


    # with this method we are looking for the most realistic value of the output
    R.save_tiff(resDST, denorm, resDST, resDST, dtype='Float32')
    end = time.time()
    print((end - start)/60 ,'min')
    print('Super resolution processed on :', '\n',
         '{}'.format(img))
    print('Location : {}'.format(resDST))
    
    shutil.rmtree(oF)
    shutil.rmtree(tF)
    os.remove('memREZ.tif')
    os.remove('stdMEM.tif')
    os.remove('mem.tif')
    return denorm