# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 11:09:51 2021

@author: Max
"""

from fastai.data.all import *
from fastai.vision.all import *
from fastai.vision.core import *
from fastai.vision.data import *

from torch.nn import Sequential, Conv2d, BatchNorm2d, ReLU, MaxPool2d
def VGG(nc) :
    return Sequential(Conv2d(nc, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    Conv2d(64, 64, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
                    Conv2d(64, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    Conv2d(128, 128, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(128, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
                    Conv2d(128, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    Conv2d(256, 256, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(256, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
                    Conv2d(256, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
                    Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    Conv2d(512, 512, kernel_size=(3, 3), stride=(1, 1), padding=(1, 1)),
                    BatchNorm2d(512, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True),
                    ReLU(inplace=True),
                    MaxPool2d(kernel_size=2, stride=2, padding=0, dilation=1, ceil_mode=False),
                    )

def gram_matrix(x):
    n,c,h,w = x.size()
    x = x.view(n, c, -1)
    return (x @ x.transpose(1,2))/(c*h*w)


class FeatureLoss(nn.Module):
    def __init__(self, m_feat, layer_ids, layer_wgts):
        super().__init__()
        self.m_feat = m_feat # getting the features from the model (VGG)
        self.loss_features = [self.m_feat[i] for i in layer_ids] # creating the features 
        self.hooks = hook_outputs(self.loss_features, detach=False) # grabbing all features at every steps
        self.wgts = layer_wgts
        self.metric_names = ['pixel',] + [f'feat_{i}' for i in range(len(layer_ids))
              ] + [f'gram_{i}' for i in range(len(layer_ids))]
        self.base_loss = F.l1_loss


    def make_features(self, x, clone=False):
        self.m_feat(x)
        return [(o.clone() if clone else o) for o in self.hooks.stored]
    
    def forward(self, input, target):
        out_feat = self.make_features(target, clone=True) # creating feature for target
        in_feat = self.make_features(input) # creating feature for input
        self.feat_losses = [self.base_loss(input,target)] # l1 loss between the pixels
        self.feat_losses += [self.base_loss(f_in, f_out)*w
                             for f_in, f_out, w in zip(in_feat, out_feat, self.wgts)]
        self.feat_losses += [self.base_loss(gram_matrix(f_in), gram_matrix(f_out))*w**2 * 5e3
                             for f_in, f_out, w in zip(in_feat, out_feat, self.wgts)]
        self.metrics = dict(zip(self.metric_names, self.feat_losses))
        return sum(self.feat_losses)
    
    def __del__(self): self.hooks.remove()
        