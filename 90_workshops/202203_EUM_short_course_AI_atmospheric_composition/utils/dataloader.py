# -*- coding: utf-8 -*-
"""
Created on Thu Sep 23 11:09:40 2021

@author: Max
"""
from fastai.data.all import *
from fastai.vision.all import *
from fastai.vision.core import *
from fastai.vision.data import *
from torchvision import transforms

class PProcess() : 
    def __init__(self, size, pad, coef) : # before being insert inside the dataloader the data needs to be resize
        self.size = size # each tile will be resized to the size of the target, indeed the upsampling is happening before the modelisation
        self.pad = pad
        self.coef = coef
        
    def RSIMG(self, path) : # function to open the images
        im = load_image(path)
        if self.size != None :
            R = Resize(self.size) 
            im = R(im) # the algorithm used for resizing is a bicubic interpolation
        if self.pad == True :
            Pad = CropPad(int(self.size+5), pad_mode='zeros') # the padding permit to better analyze the edges of the image
            im = Pad(im)
        timg = TensorImage(image2tensor(im)) # to be used by fastai and AI libraries in general the patches needs to be convert to tensor
        if self.coef != None :
            timg = timg * self.coef
        return timg.float()

def createDLS(Pinput, bs, chan, func, tfms, pad, coef = None, size = None):
    """
    Pinput : Path to the input dataset
    size : size of the target to retrieve
    bs : batch size
    chan : number of channel
    func : function to retrieve the target images based on the input
    """
    P=PProcess(size, pad, coef) # set up the image opening based on the target size
    comp = setup_aug_tfms(tfms) 
    db = DataBlock( # creation of a dabablock for opening images / converting to tensor / splitting and data augmentation
                  blocks = (TransformBlock(P.RSIMG), # creating a TransformBlock using the function made to open the images
                             TransformBlock(P.RSIMG)),
                  get_items = get_image_files, # os.listdir(Pinput) to get filenames
                  get_y = func, # retrieve the corresponding target
                  splitter = RandomSplitter(0.1), # splitting between training and validation (10% of the dataset)
                  batch_tfms = comp
                  )

    ds = db.datasets(Pinput) # extracting the dataset from the datablocks
    dls = db.dataloaders(Pinput, # creating the dataloader from the parameters of the datablock
                    batch_size=bs, # setting up the batch size to split the training dataset for each epoch
                    num_workers = 0, # load the data inside the main process
                    drop_last = False) # don't drop the last batch even if it is not at its maximum size
    return dls # retrieve the dataset