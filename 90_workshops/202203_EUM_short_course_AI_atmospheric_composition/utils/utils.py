#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 14:18:48 2020

@author: maximilienhouel
"""

import os
import datetime
from osgeo import gdal
import shutil
import requests
    
def downloadURL(url, dst):
    r = requests.get(url)
    print(r)
    print(r.content)
    if not os.path.isfile(dst) :
        with open(dst, 'wb') as F:
            F.write(r.content)
            F.close
            
def flatten(array) :
    flat = array.reshape(array.shape[0]*array.shape[1])
    return flat

def delete_folder(path) :
    shutil.rmtree(path)

def delete_file(path) :
    os.remove(path)

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days)):
        yield start_date + datetime.timedelta(n)

def normalize(array, minimum, maximum):
    return (array - minimum)/(maximum - minimum)

def translate_tif_png(src,dst):
    gdal.Translate(destName = dst,
                   srcDS = src,
                   format = 'PNG',
                   outputType = gdal.GDT_Byte,
                   scaleParams = [0, 255])



