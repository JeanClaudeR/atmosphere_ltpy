{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src='./img/EU-Copernicus-EUM_3Logos.png' alt='Logo EU Copernicus EUMETSAT' align='right' width='50%'></img>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Monitoring fires with next-generation satellites from MTG and Metop-SG: Gironde, France Wildfires Case Study"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "In the near future, new satellites such as [Meteosat Third Generation (MTG)](https://www.eumetsat.int/meteosat-third-generation) and [Metop - Second Generation (Metop-SG)](https://www.eumetsat.int/metop-sg) will provide advanced capabilities and valuable data for monitoring fires and their impacts. This case study will introduce upcoming data products from MTG and Metop-SG in the context of the wildfires in Gironde, France in July and August 2022 and show how these upcoming products, alongside complementary existing data products, can be used to monitor the full fire life cycle.\n",
    "\n",
    "## The Case Event\n",
    "According to [Al Jazeera](https://www.aljazeera.com/news/2022/8/10/thousands-evacuated-from-frances-gironde-as-forest-fires-rage), \"The Gironde was hit by major wildfires in July which destroyed more than 20,000 hectares (49,421 acres) of forest and temporarily forced nearly 40,000 people from their homes.\" These fires continued to burn in August, leading to the deployment of over 1,000 firefighters to try and control the spread. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<center><img src='./img/aljazeera_gironde.jpg' alt='Gironde, France' width='70%'></img>\n",
    "<br><i>Flames consume trees at a forest fire in Saint Magne, south of Bordeaux <a href=\"https://www.aljazeera.com/news/2022/8/10/thousands-evacuated-from-frances-gironde-as-forest-fires-rage\"> (SDIS 33 Service Audiovisuel via AP) </a></i>\n",
    "</center>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Meteosat Third Generation (MTG)\n",
    "\n",
    "MTG will see the launch of six new geostationary satellites from 2022 onwards. The satellite series will be based on 3-axis platforms and comprise: four imaging satellites (MTG-I) and two sounding satellites (MTG-S). The full operational configuration will consist of two MTG-I satellites operating in tandem, one scanning Europe and Africa every 10 minutes, and the other scanning only Europe every 2.5 minutes, and one MTG-S satellite. \n",
    "\n",
    "## EUMETSAT Polar System - Second Generation (EPS-SG)\n",
    "\n",
    "The EPS-SG is a partnership programme, which involves the European Space Agency, the German Aerospace Agency and the National Center for Space Studies. The mission is composed of two series of spacecraft, Metop-SG A and B, flying on the same mid-morning orbit, like the current Metop satellites. The orbit height is in the range 823-848 km (dependent on latitudes). There will be three satellites each of Metop-SG A and Metop-SG B. The first two satellites Metop-SG A1 and Metop-SG B1 are planned to be launched in Q2 and Q4 2024 respectively. See [here](https://www.eumetsat.int/our-satellites/metop-series?sjid=future) an overview of the planned launch dates. \n",
    "\n",
    "Consisting of low earth orbiting satellites, Metop-SG will provide high spatial resolution imagery and data. However, they will only cover a smaller observation region and have a longer revisit period compared to geostationary satellites like MTG. \n",
    "\n",
    "In this case study, we will be focusing on data from several instruments, namely MTG’s [Flexible Combined Imager](https://www.eumetsat.int/mtg-flexible-combined-imager), [Sentinel-4 UVNS instrument](https://sentinels.copernicus.eu/web/sentinel/missions/sentinel-4/instrumental-payload) and Metop-SG’s [METimage](https://www.eumetsat.int/eps-sg-metimage) radiometer and the [Copernicus Sentinel-5 UVNS spectrometer](https://sentinels.copernicus.eu/web/sentinel/missions/sentinel-5). We will also be using data from the Sentinel-2 [MultiSpectral Instrument](https://sentinel.esa.int/web/sentinel/missions/sentinel-2/instrument-payload) to visualise the burn scars from the fires. While data from MTG and Metop-SG is not yet available, we will be using proxy data from existing instruments to demonstrate the new capabilities that will arise from these satellites. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-warning\">\n",
    "\n",
    "<b>What is proxy data? </b>\n",
    "\n",
    "Proxy data refers to “data with valid scientific content, to be used in early training on instrument capabilities and application areas, for example in test beds. These are real datasets from relevant existing precursor instruments.” (Source: [EUMETSAT](https://www.eumetsat.int/media/43504)) \n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### MTG Flexible Combined Imager\n",
    "\n",
    "#### <a id='gironde_fig1'></a>Fire risk mapping\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "Early information for fire risk is crucial for emergency response. One important source of information is the [Fire Risk Map v2 (FRMv2)](https://landsaf.ipma.pt/en/products/fire-products/frmv2/), which consists of daily forecasts of classes of meteorological fire danger over Mediterranean Europe. Five fire risk classes are provided, ranging from low to extreme risk. The forecast timesteps available start at 24 hours to 120 hours. `Figure 1` shows the fire risk map over southern France for 18 July 2022. The forecast data was retrieved with a 24 hour time step from 18 July 2022. Southern France is mostly coloured in red and dark red, showing very high to extreme fire risk. \n",
    "\n",
    "\n",
    "Currently, this dataset is based on LSA SAF data records of daily released energy by active fires derived from [Meteosat Second Generation's SEVIRI instrument](https://www.eumetsat.int/seviri). In the future, data from [Flexible Combined Imager (FCI)](https://www.eumetsat.int/mtg-flexible-combined-imager) aboard the MTG satellites will be incorporated into fire risk mapping. The FCI provides 16 channels in the visible and infrared spectrum with a spatial sampling distance in the range of 1 to 2 km and four channels with a spatial sampling distance in the range of 0.5 to 1 km. The increased spatial resolution of FCI will allow the detection of less intense fires than with the current SEVIRI pixel size. Also the dynamic range of the middle infrared FCI channel will be enhanced, avoiding pixel saturation for very intense fires. The quality of the FRMv2 product will be improved through better calibration."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='./img/gironde_lsasaf_frm.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 1. 24-hour forecast of fire risk for 18 July 2022 from LSA SAF over southern France.\n",
    "<br>Learn how this [plot of fire risk](./01_LSA_SAF_FRMv2.ipynb) was made.</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### <a id='gironde_fig2'></a> Fire Radiative Power"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "The [Fire Radiative Power Pixel (FRPPIXEL)](https://landsaf.ipma.pt/en/products/fire-products/frppixel/) product records information on the location, timing and fire radiative power (FRP, in megawatts) output of wildfires detected every 15 minutes across the full MSG disk at the native spatial resolution of the SEVIRI sensor. This enables emergency responders to pinpoint where active fires are burning and have information about how intense these fires are.\n",
    "\n",
    "`Figure 2` shows the fire radiative power in megawatts (M W) over France on 18 July 2022. The proxy data source is the near real-time [MODIS Thermal Anomalies/Fire locations 1km FIRMS V0061 (MCD14DL)](https://earthdata.nasa.gov/earth-observation-data/near-real-time/firms/mcd14dl) dataset. While the fire radiative power data is continuous, this visualisation groups them into categories based on their intensity. High intensity fires with fire radiative power of 120 to 500 MW are shown in bright red, while low intensity fires of up to 30 MW are shown in blue. The fires on Evia island are visible in bright red whereas most of the fires in southern Italy are colored blue as they are lower in intensity.\n",
    "\n",
    "In the future, data from MTG’s [Flexible Combined Imager (FCI)](https://www.eumetsat.int/mtg-flexible-combined-imager) instrument will be used for the FRPPIXEL product. The FCI provides 16 channels in the visible and infrared spectrum with a spatial sampling distance in the range of 1 to 2 km and four channels with a spatial sampling distance in the range of 0.5 to 1 km. It will have better spatial, temporal and radiometric resolution than MSG SEVERI, so even smaller fires will be detected. The data provider is LSA SAF, which develops and processes satellite products that characterise the continental surfaces, such as radiation products, vegetation, evapotranspiration and wildfires."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='./img/gironde_modis_frp.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 2. Fire radiative power from MODIS Thermal Anomalies/ Fire Locations data over Gironde, France on 18 July 2022.\n",
    "<br>Learn how this [plot of fire radiative power](./02_MODIS_FRP.ipynb) was made.</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### EPS-SG Sentinel-5 - UVNS"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### <a id='gironde_fig3'></a> Ultraviolet Aerosol Index\n",
    "\n",
    "Monitoring trace gases and aerosols is important for understanding smoke transport patterns. Metop-SG A will carry the [Copernicus Sentinel-5 UVNS spectrometer](https://sentinels.copernicus.eu/web/sentinel/missions/sentinel-5) for measurements of trace gases which will also improve air quality forecasts produced by CAMS. The UVNS instrument will provide ultraviolet aerosol index (UVAI) information as a Level 2 product. The Gironde, France wildfires caused elevated aerosol levels in the atmosphere as shown by the higher values of the UVAI Level 2 product in `Figure 3`. The proxy data comes from [Sentinel-5P](https://sentinels.copernicus.eu/web/sentinel/missions/sentinel-5p), the precursor instrument for Sentinel-5. \n",
    "\n",
    "Compared to its predecessor, [GOME-2](https://www.eumetsat.int/gome-2), the major breakthrough of Sentinel-5 will be a drastically increased spatial sampling at a sampling distance of 7km, and the extension of measurements into the near and short-wave infrared, providing information about aerosols, as well as methane and carbon monoxide in the planetary boundary layer."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='./img/gironde_s5p_uvai.png'  width='60%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 3. UVAI Level 2 product from Sentinel-5P TROPOMI over Gironde, France recorded on 10 August 2022.\n",
    "<br>Learn how this [UVAI plot](./03_Sentinel-5P_TROPOMI_UVAI.ipynb) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### <a id='gironde_fig4'></a>Carbon monoxide total column\n",
    "\n",
    "The Sentinel-5 UVNS instrument will also be able to monitor the elevated carbon monoxide content in the atmosphere, especially during large wildfires. Carbon monoxide total column data will be provided as a Level 2 product. In `Figure 4`, the dark blue areas of the plot of total column carbon monoxide show that the background value is around 2<sup>-18</sup> molec./cm<sup>2</sup>. The yellow and green areas of the plot show plumes of carbon monoxide with up to 3 to 4 times the background value. This corresponds to plumes of smoke from the wildfires in Gironde, France."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='./img/gironde_s5p_co.png'  width='60%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 4. Carbon monoxide (CO) total column Level 2 product from Sentinel-5P TROPOMI over Gironde, France recorded on 10 August 2022.\n",
    "<br>Learn how this [carbon monoxide total column plot](./04_Sentinel-5P_TROPOMI_CO.ipynb) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### MTG Sentinel-4 - UVNS \n",
    "\n",
    "#### <a id='gironde_fig5'></a>  Nitrogen dioxide total column\n",
    "\n",
    "Nitrogen dioxide is another useful indicator of smoke presence and transport. `Figure 5` shows an animation of simulated nitrogen dioxide Level 2 data from Sentinel-4 with hourly data for 24 hours on 10 August 2022. Elevated levels of nitrogen dioxide are visible in Gironde where the FRP data shown in `Figure 2` indicates fire locations.\n",
    "\n",
    "The geostationary Sentinel-4 mission will provide hourly data on tropospheric constituents over Europe for air quality applications. The target species of the Sentinel-4 mission include key air quality parameters such as nitrogen dioxide, ozone, sulphur dioxide, formaldehyde, glyoxal, and aerosols. These species will be monitored by the [Sentinel-4 UVNS instrument](https://sentinels.copernicus.eu/web/sentinel/missions/sentinel-4/instrumental-payload) aboard the MTG Sounder satellite."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "tags": [
     "hide_input"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "\n",
       "    <div align=\"center\">\n",
       "    <video width=\"1200\" height=\"1000\" alt=\"figure4\" controls>\n",
       "        <source src=\"./img/2022-08-10_S4_NO2.mp4\" type=\"video/mp4\">\n",
       "    </video></div>\n"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from IPython.display import HTML\n",
    "\n",
    "HTML(\"\"\"\n",
    "    <div align=\"center\">\n",
    "    <video width=\"1200\" height=\"1000\" alt=\"figure4\" controls>\n",
    "        <source src=\"./img/2022-08-10_S4_NO2.mp4\" type=\"video/mp4\">\n",
    "    </video></div>\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 5. Simulated nitrogen dioxide Level 2 product from Sentinel-4 UVNS spectrometer over Gironde, France recorded hourly for 24 hours on 10 August 2022.\n",
    "<br>Learn how this animation of [simulated nitrogen dioxide data from Sentinel-4](./05_Sentinel-4_UVNS_NO2.ipynb) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "## Sentinel-2 \n",
    "Data from [Sentinel-2](https://sentinel.esa.int/web/sentinel/missions/sentinel-2/) is especially useful for assessing burn severity due to its high spatial resolution of up to 10m. This makes it a useful complementary dataset to the data from MTG and Metop-SG. We will now demonstrate several ways that data from Sentinel-2 can be used, including the creation of composite imagery, calculation of a burn index and burn severity mapping. \n",
    "\n",
    "The Copernicus Sentinel-2 mission comprises a constellation of two polar-orbiting satellites placed in the same sun-synchronous orbit, phased at 180° to each other. It aims at monitoring variability in land surface conditions, and its wide swath width (290 km) and high revisit time (10 days at the equator with one satellite, and 5 days with 2 satellites under cloud-free conditions which results in 2-3 days at mid-latitudes) will support monitoring of Earth's surface changes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### Sentinel-2 MultiSpectral Instrument\n",
    "The [Sentinel-2 MultiSpectral Instrument (MSI)](https://sentinel.esa.int/web/sentinel/missions/sentinel-2/instrument-payload) has 13 spectral bands which provide data for land cover/change classification, atmospheric correction and cloud/snow separation. The MSI samples four bands at 10 m, six bands at 20 m and three bands at 60 m spatial resolution. Due to the high spatial resolution of up to 10m, the Sentinel-2 MSI data is useful for creating image composites. MSI data products include top-of-atmosphere reflectances (Level 1C data) and bottom-of-atmosphere reflectances (Level 2A data). \n",
    "\n",
    "#### <a id='gironde_fig6'></a>Natural colour composites\n",
    "Natural colour composites are similar to what the human eye would see. To create a natural colour composite, one can assign the red, green and blue channels of image to show the corresponding visible red, visible green and visible blue bands of the satellite data. `Figure 6` shows a natural colour composite from Sentinel-2 Level 2A data from 20 September 2022 after the fires were extinguished."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='./img/gironde_s2_natural.png' align='centre' width='60%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 6. Natural colour composite from Sentinel-2 Level 2A data showing post-fire conditions on 20 September 2022 in Gironde, France. \n",
    "<br>Learn how this [natural colour composite](./06_Sentinel-2.ipynb#s2_natural) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### <a id='gironde_fig7'></a> False colour composite\n",
    "\n",
    "The burn scars from the fires are not easily seen in the natural colour image in `Figure 6`. However, we can also create a false colour composite that makes burn scars more easily distinguishable. The recipe for the false colour composites is: `Red-Green-Blue 12-8A-2`. This means that the red channel was assigned to band 12 (shortwave infrared), the green channel was assigned to band 8A (near-infrared) and the blue channel was assigned to band 2 (the visible blue band). This recipe highlights burn scars in brown and healthy vegetation in green. \n",
    "\n",
    "`Figure 7` shows a false colour composite created from the Sentinel-2 MSI data captured after the fires were extinguished on 20 September 2022."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='./img/gironde_s2_false.png' align='centre' width='60%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 7. False colour composites from Sentinel-2 Level 2A data showing post-fire conditions on 20 September 2022 in Gironde, France.\n",
    "<br>Learn how this [false colour composite](./06_Sentinel-2.ipynb#s2_false) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the 2022 Gironde, France wildfires as an example, we introduced upcoming data products that will be available on the new MTG and Metop-SG satellites which are useful for monitoring the entire fire life cycle."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "Collapsed": "false"
   },
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### References\n",
    "* Trigo, I. F., C. C. DaCamara, P. Viterbo, J.-L. Roujean, F. Olesen, C. Barroso, F. Camacho-de Coca, D. Carrer, S. C. Freitas, J. García-Haro, B. Geiger, F. Gellens-Meulenberghs, N. Ghilain, J. Meliá, L. Pessanha, N. Siljamo, and A. Arboleda. (2011). The Satellite Application Facility on Land Surface Analysis. Int. J. Remote Sens., 32, 2725-2744, doi: 10.1080/01431161003743199\n",
    "* MODIS Collection 61 Hotspot / Active Fire Detections MCD14ML distributed from NASA FIRMS. Available on-line https://earthdata.nasa.gov/firms. doi:10.5067/FIRMS/MODIS/MCD14ML\n",
    "* Generated using Copernicus Atmosphere Monitoring Service Information 2022\n",
    "* Hersbach, H., Bell, B., Berrisford, P., Biavati, G., Horányi, A., Muñoz Sabater, J., Nicolas, J., Peubey, C., Radu, R., Rozum, I., Schepers, D., Simmons, A., Soci, C., Dee, D., Thépaut, J-N. (2018): ERA5 hourly data on single levels from 1959 to present. Copernicus Climate Change Service (C3S) Climate Data Store (CDS). 10.24381/cds.adbb2d47\n",
    "* Copernicus Sentinel data 2022"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p style=\"text-align:right;\">This project is licensed under the <a href=\"./LICENSE\">GPL-3.0 License</a> and is developed under a Copernicus contract."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
