{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "Collapsed": "false"
   },
   "source": [
    "<img src='../img/joint_school_banner_2.png' alt='Training school and workshop on fire' align='center' width='100%'></img>\n",
    "\n",
    "<br>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Atmospheric Impacts of Wildfires"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "This section will introduce data products for monitoring the atmospheric impacts of wildfires, in the context of the summer wildfires which occurred in southern Italy and Greece in early August 2021. In particular, we will be introducing the false colour composites, the ultraviolet aerosol index, fire radiative power and nitrogen dioxide total column data.\n",
    "\n",
    "## The Case Event\n",
    "A long period of warm and dry weather led to fires breaking out across the Mediterranean during the summer of 2021. In the Mediterranean region, the winters are rainy and mild while the summers are hot and dry, leading to vegetation stress and the accumulation of fuels in the summer months. \n",
    "\n",
    "In Greece, the largest wildfires occurred in Attica, Olympia, Messenia, and Evia (also known as Euboea). The fires in Evia, Greece's second largest island, were particularly devastating. They led to two deaths and <a href='https://www.eumetsat.int/smoke-and-burned-areas-greek-fires' target='_blank'>destroyed 125,000 hectares of land</a>. \n",
    "\n",
    "Southern Italy and Sicily were particularly impacted by wildfires. In Calabria, fires restarted in early August in the Aspromonte Mountain around the San Luca Valley, <a href='https://emergency.copernicus.eu/mapping/list-of-components/EMSR534' target='_blank'>threatening ancient and primaeval beech forests</a> (Faggete vetuste) that are recognised as a UNESCO World Heritage site. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### <a id='s3_olci'></a> False colour composites from Sentinel-3 data\n",
    "\n",
    "<a href='https://sentinels.copernicus.eu/web/sentinel/missions/sentinel-3/instrument-payload/olci' target='_blank'>Sentinel-3 Ocean and Land Colour Imager (OLCI)</a> produces high resolution imagery for monitoring active fire and smoke transport impacts. Sentinel-3 OLCI has a spatial resolution of 300m and <a href='https://sentinels.copernicus.eu/web/sentinel/user-guides/sentinel-3-olci/resolutions/radiometric' target='_blank'>21 bands</a>. `Figure 1` shows a false colour composite from 7 August 2021 where smoke plumes from the Greek fires in Evia are visible. The recipe for the false colour composites is: `Red-Green-Blue Oa17-Oa05-Oa02`. This means that the red channel was assigned to band Oa17 (865 nm), the green channel was assigned to band Oa05 (510 nm) and the blue channel was assigned to band Oa02 (412.5nm). This false colour combination highlights healthy vegetation as red and burnt areas as black.  "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part2_figure_1.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 1. False colour composite from Sentinel-3 OLCI Level-1B data showing smoke plumes from Evia, Greece recorded on 7 August 2021.\n",
    "<br>Learn how this [false colour composite](./s3_olci.ipynb) was made.</i></i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### <a id='modis_frp'></a> Fire Radiative Power"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "The <a href='https://landsaf.ipma.pt/en/products/fire-products/frppixel/' target='_blank'>Fire Radiative Power Pixel (FRPPIXEL)</a> product records information on the location, timing and fire radiative power (FRP, in megawatts) output of wildfires detected every 15 minutes across the full MSG disk at the native spatial resolution of the SEVIRI sensor. This enables emergency responders to pinpoint where active fires are burning and have information about how intense these fires are.\n",
    "\n",
    "`Figure 2` shows the fire radiative power in megawatts (M W) over Italy and Greece from 5 August 2021. The proxy data source is the near real-time <a href='https://earthdata.nasa.gov/earth-observation-data/near-real-time/firms/mcd14dl' target='_blank'>MODIS Thermal Anomalies/Fire locations 1km FIRMS V0061 (MCD14DL)</a> dataset. While the fire radiative power data is continuous, this visualisation groups them into categories based on their intensity. High intensity fires with fire radiative power of 120 to 500 MW are shown in bright red, while low intensity fires of up to 30 MW are shown in blue. The fires on Evia island are visible in bright red whereas most of the fires in southern Italy are colored blue as they are lower in intensity.\n",
    "\n",
    "In the future, data from Meteosat Third Generation’s <a href='https://www.eumetsat.int/mtg-flexible-combined-imager' target='_blank'>Flexible Combined Imager (FCI)</a> instrument will be used for the FRPPIXEL product. The FCI provides 16 channels in the visible and infrared spectrum with a spatial sampling distance in the range of 1 to 2 km and four channels with a spatial sampling distance in the range of 0.5 to 1 km. It will have better spatial, temporal and radiometric resolution than MSG SEVERI, so even smaller fires will be detected. The data provider is LSA SAF, which develops and processes satellite products that characterise the continental surfaces, such as radiation products, vegetation, evapotranspiration and wildfires."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part2_figure_2.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "<i>Figure 2. Fire radiative power from MODIS Thermal Anomalies/ Fire Locations data over Italy and Greece from 5 August 2021\n",
    "<br>Learn how this [plot of fire radiative power](./modis_frp.ipynb) was made.</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### <a id='s5p_ai'></a> Ultraviolet Aerosol Index\n",
    "\n",
    "Monitoring trace gases and aerosols is important for understanding smoke transport patterns. <a href='https://sentinels.copernicus.eu/web/sentinel/missions/sentinel-5p' target='_blank'>Sentinel-5P</a> carries TROPOMI (TROPOspheric Monitoring Instrument) for measurements of trace gases. Sentinel-5P TROPOMI provides ultraviolet aerosol absorption index information as a Level 2 product. The Greece and Italy wildfires caused an elevated presence of aerosols in the atmosphere as shown by the higher values of the Ultraviolet Aerosol Index Level 2 product in `Figure 3`. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part2_figure_4.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 3. Ultraviolet Aerosol Index (UVAI) Level 2 product from Sentinel-5P TROPOMI over Italy and Greece recorded on 5 August 2021.\n",
    "<br>Learn how this [aerosol index plot](./s5p_ai.ipynb) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### <a id='cams_co'></a>Carbon monoxide volume mixing ratio\n",
    "\n",
    "CAMS also provides carbon monoxide, an indicator of smoke, as a data variable over multiple pressure levels. We can calculate the volume mixing ratio at each pressure level over an area (meridional mean). In `Figure 4`, the dark red areas show that most of the CO concentrations from the Mediterranean wildfires in early August 2021 were transported at atmospheric pressure levels between 800 to 1000 millibars. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/cams_co_meridionalmean.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 4. Cross-section plot showing the meridional mean of carbon monoxide volume mixing ratio over the Mediterranean on 9 August 2021.\n",
    "<br>Learn how this [carbon monoxide cross-section plot](./cams_co.ipynb) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In addition, we can also calculate a zonal mean of the carbon monoxide mixing ratio which allows us to better understand the background values in the Mediterranean region, as shown in `Figure 5`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "<div align=\"center\"><img src='../img/cams_co_zonalmean.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 5. Cross-section plot showing the zonal mean of carbon monoxide volume mixing ratio over the Mediterranean on 9 August 2021.\n",
    "<br>Learn how this [carbon monoxide cross-section plot](./cams_co.ipynb) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### <a id='s4_no2'></a> Nitrogen dioxide total column\n",
    "\n",
    "Nitrogen dioxide is another useful indicator of smoke presence and transport. `Figure 6` shows an animation of simulated nitrogen dioxide Level 2 data from Sentinel-4 with hourly data for 24 hours on 7 August 2021. Elevated levels of nitrogen dioxide are visible over Italy and Greece where the FRP data shown in `Figures 2` indicate fire locations.\n",
    "\n",
    "The geostationary Sentinel-4 mission will provide hourly data on tropospheric constituents over Europe for air quality applications. The target species of the Sentinel-4 mission include key air quality parameters such as nitrogen dioxide, ozone, sulphur dioxide, formaldehyde, glyoxal, and aerosols. These species will be monitored by the <a href='https://sentinels.copernicus.eu/web/sentinel/missions/sentinel-4/instrumental-payload' target='_blank'>Sentinel-4 UVNS instrument</a> aboard the MTG Sounder satellite."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "jupyter": {
     "source_hidden": true
    },
    "tags": [
     "hide_input"
    ]
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "\n",
       "    <div align=\"center\">\n",
       "    <video width=\"800\" height=\"600\" alt=\"figure6\" controls>\n",
       "        <source src=\"../img/med_part2_figure_6.mp4\" type=\"video/mp4\">\n",
       "    </video></div>\n"
      ],
      "text/plain": [
       "<IPython.core.display.HTML object>"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "from IPython.display import HTML\n",
    "\n",
    "HTML(\"\"\"\n",
    "    <div align=\"center\">\n",
    "    <video width=\"800\" height=\"600\" alt=\"figure6\" controls>\n",
    "        <source src=\"../img/med_part2_figure_6.mp4\" type=\"video/mp4\">\n",
    "    </video></div>\n",
    "\"\"\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 6. Simulated nitrogen dioxide Level 2 product from Sentinel-4 UVNS spectrometer over the Mediterranean recorded hourly for 24 hours on 7 August 2021.\n",
    "<br>Learn how this animation of [simulated nitrogen dioxide data from Sentinel-4](./s4_no2.ipynb) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "Collapsed": "false"
   },
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "Collapsed": "false"
   },
   "source": [
    "<p style=\"text-align:right;\">This project is licensed under the <a href=\"./LICENSE\">MIT License</a> and is developed under a Copernicus contract."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
