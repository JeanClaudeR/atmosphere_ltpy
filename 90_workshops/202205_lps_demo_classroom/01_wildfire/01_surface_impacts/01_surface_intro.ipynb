{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<img src='../img/joint_school_banner_2.png' alt='training school and workshop on fire' align='right' width='100%'></img>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "# Surface Impacts of Wildfires"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "This section will introduce data products for monitoring the surface impacts of wildfires, in the context of the summer wildfires which occurred in southern Italy and Greece in early August 2021. In particular, we will be covering fire weather index data, fire risk, fire radiative power and the Normalised Burn Ratio for assessing burn severity.\n",
    "\n",
    "## The Case Event\n",
    "A long period of warm and dry weather led to fires breaking out across the Mediterranean during the summer of 2021. In the Mediterranean region, the winters are rainy and mild while the summers are hot and dry, leading to vegetation stress and the accumulation of fuels in the summer months. \n",
    "\n",
    "In Greece, the largest wildfires occurred in Attica, Olympia, Messenia, and Evia (also known as Euboea). The fires in Evia, Greece's second largest island, were particularly devastating. They led to two deaths and <a href='https://www.eumetsat.int/smoke-and-burned-areas-greek-fires' target='_blank'>destroyed 125,000 hectares of land</a>. \n",
    "\n",
    "Southern Italy and Sicily were particularly impacted by wildfires. In Calabria, fires restarted in early August in the Aspromonte Mountain around the San Luca Valley, <a href='https://emergency.copernicus.eu/mapping/list-of-components/EMSR534' target='_blank'>threatening ancient and primaeval beech forests</a> (Faggete vetuste) that are recognised as a UNESCO World Heritage site. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### <a id='geff_fwi'></a> Fire Weather Index\n",
    "The <a href='https://www.ecmwf.int/' target='_blank'> European Centre for Medium-Range Weather Forecasts (ECMWF)</a> produces daily fire danger forecasts and reanalysis products for the <a href='https://emergency.copernicus.eu/' target='_blank'>Copernicus Emergency Management Services (CEMS)</a>. The modelling system that generates the fire data products is called <a href='https://git.ecmwf.int//projects/CEMSF/repos/geff/browse' target='_blank'>Global ECMWF Fire Forecast (GEFF)</a> and it is based on the Canadian Fire Weather index as well as the US and Australian fire danger systems. As GEFF uses CAMS data for modelling, both GEFF and CAMS will benefit from the inclusion of the higher spatial, temporal and radiometric resolution of the future data from MTG and EPS-SG. \n",
    "\n",
    "`Figure 1` shows the <a href='https://git.ecmwf.int/projects/CEMSF/repos/geff/browse' target='_blank'>GEFF Fire Weather Index (FWI)</a> for 5th August 2021 as classified using European Fire Danger Classes. While the FWI values are continuous, `Figure 1` groups them into fire weather categories ranging from very low (green), low (yellow), moderate (orange), high (red), very high (brown) to extreme fire danger (black). Greece and Italy both have areas that have very high and extreme fire danger. Learn how this fire weather index plot was made."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part1_figure_5.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 1. The GEFF Fire Weather Index for 5th August 2021 as classified using European Fire Danger Classes.\n",
    "<br>Learn how this [fire weather index plot](./geff_fwi.ipynb) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <a id='lsasaf_frm'></a> Fire Risk Map: fire risk, fire probability and fire probability anomalies"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "Early information for fire risk is crucial for emergency response. One important source of information is the <a href='https://landsaf.ipma.pt/en/products/fire-products/frmv2/' target='_blank'>Fire Risk Map v2 (FRMv2)</a>, which consists of daily forecasts of classes of meteorological fire danger over Mediterranean Europe. Five fire risk classes are provided, ranging from low to extreme risk. The forecast timesteps available start at 24 hours to 120 hours. `Figure 2` shows the fire risk map over Italy and Greece for 5th August 2021. The forecast data was retrieved with a 24 hour time step from 4th August 2021. Greece is mostly coloured in dark red, showing extreme fire risk. Italy has multiple areas coloured in orange which indicates high fire risk or bright red for very high fire risk. Currently, this dataset is based on LSA SAF data records of daily released energy by active fires derived from <a href='https://www.eumetsat.int/seviri' target='_blank'>Meteosat Second Generation's SEVIRI instrument</a>. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part1_figure_2.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 2. 24-hour forecast of fire risk for 5th August 2021 from LSA SAF.\n",
    "<br>Learn how this [plot of fire risk](./lsasaf_frm.ipynb) was made.</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <a id='lsasaf_p2000'></a>  Probabilities of a Potential Wildfire exceeding 2000 GJ of Daily Energy released by Fires (P2000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another data variable available within the FRMv2 dataset is P2000, which shows the probabilities of a potential wildfire exceeding 2000 GJ of daily energy released by fires (P2000). In other words, it provides information on the likelihood of a potential wildfire getting out of control. `Figure 3` shows the 24 hour forecast of P2000 for 5th August 2021. Greece is mostly coloured in orange and red, indicating a high probability of a potential wildfire getting out of control. Italy is mostly coloured in blue and green, indicating lower probabilities of a wildfire getting out of control."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part1_figure_3.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 3. 24-hour forecast of the probability of fire emitted energy being above 2000 GJ for 5th August 2021 from LSA SAF.\n",
    "<br>Learn how this [plot of P2000](./lsasaf_frm.ipynb) was made.</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### <a id='lsasaf_p2000a'></a> 24 hour forecast of the anomaly of probability of exceedance of 2000 GJ of daily energy released by fires (P2000a)\n",
    "\n",
    "`Figure 4` shows the 24 hour forecast of the anomaly of probability of exceedance of 2000 GJ of daily energy released by fires (P2000a) for 5th August 2021. This quantity is computed at each pixel as the deviation of the probability of exceedance for a given pixel and a given day of a given year from the average of all values of probability of exceedance for that pixel and day over the period 1979–2016. Greece is mostly coloured in dark and light red, indicating that there is a positive anomaly for the probability of a potential wildfire getting out of control. Southern Italy is also mostly coloured in light red. This means that it is more likely than usual for a fire to burn out of control in both countries."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part1_figure_4.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 4. 24-hour forecast of the anomalies of the probability of fire emitted energy being above 2000 GJ for 5th August 2021, from LSA SAF.\n",
    "<br>Learn how this [plot of P2000a](./lsasaf_frm.ipynb) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### <a id='cams_gfas'></a> Fire Radiative Power"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The <a href='https://atmosphere.copernicus.eu/global-fire-emissions' target='_blank'>CAMS Global Fire Assimilation System (GFAS)</a> is a useful source of fire radiative power data. `Figure 5` shows the gridded fire radiative power dataset from CAMS GFAS. GFAS assimilates fire radiative power observations from satellite-based sensors to produce daily estimates of wildfire and biomass burning emissions. It also provides information about injection heights derived from fire observations and meteorological information from the operational weather forecasts of ECMWF. The GFAS data output includes spatially gridded fire radiative power, dry matter burnt and biomass burning emissions for a large set of chemical, greenhouse gas and aerosol species. Data are available globally on a regular latitude-longitude grid with horizontal resolution of 0.1 degrees from 2003 to present. \n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part2_figure_3.png'  width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 5. Fire radiative power from CAMS GFAS over Italy and Greece from 5 August 2021.\n",
    "<br>Learn how this [plot of fire radiative power](./cams_gfas.ipynb) was made.</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "### <a id='s2_msi'></a> Normalised Burn Ratio from Sentinel-2 data \n",
    "Data from <a href='https://sentinel.esa.int/web/sentinel/missions/sentinel-2/' target='_blank'>Sentinel-2</a> is especially useful for assessing burn severity due to its high spatial resolution of up to 10m. We will now demonstrate several ways that data from Sentinel-2 can be used, including the creation of composite imagery, calculation of a burn index and burn severity mapping. \n",
    "\n",
    "The Copernicus Sentinel-2 mission comprises a constellation of two polar-orbiting satellites placed in the same sun-synchronous orbit, phased at 180° to each other. It aims at monitoring variability in land surface conditions, and its wide swath width (290 km) and high revisit time (10 days at the equator with one satellite, and 5 days with 2 satellites under cloud-free conditions which results in 2-3 days at mid-latitudes) will support monitoring of Earth's surface changes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "The <a href='https://sentinel.esa.int/web/sentinel/missions/sentinel-2/instrument-payload' target='_blank'>Sentinel-2 MultiSpectral Instrument (MSI)</a> has 13 spectral bands which provide data for land cover/change classification, atmospheric correction and cloud/snow separation. The MSI samples four bands at 10 m, six bands at 20 m and three bands at 60 m spatial resolution. Due to the high spatial resolution of up to 10m, the Sentinel-2 MSI data is useful for creating image composites. MSI data products include top-of-atmosphere reflectances (Level 1C data) and bottom-of-atmosphere reflectances (Level 2A data). \n",
    "\n",
    "#### <a id='s2_natural'></a> Natural colour composites\n",
    "Natural colour composites are similar to what the human eye would see. To create a natural colour composite, one can assign the red, green and blue channels of image to show the corresponding visible red, visible green and visible blue bands of the satellite data. `Figure 6` shows natural colour composites from Sentinel-2 Level 2A data on two dates. The left image was taken before the fires on 1 August 2021 and the image on the right was taken after the fires were extinguished on 25 September 2021. The island on the bottom-right corner of the image is the Greek island of Evia. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part3_figure_2.png' width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 6. Natural colour composites from Sentinel-2 Level 2A data showing pre-fire conditions on 1 August 2021 (left) and post-fire conditions on 25 September 2021 (right) in Greece. \n",
    "<br>Learn how these [natural colour composites](./s2_dnbr.ipynb#s2_natural_21) were made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### <a id='s2_false'></a> False colour composites\n",
    "\n",
    "The burn scars from the fires are not easily seen in the natural colour images in `Figure 6`. However, we can also create a false colour composite that makes burn scars more easily distinguishable. The recipe for the false colour composites is: `Red-Green-Blue 12-8A-2`. This means that the red channel was assigned to band 12 (shortwave infrared), the green channel was assigned to band 8A (near-infrared) and the blue channel was assigned to band 2 (the visible blue band). This recipe highlights burn scars in brown and healthy vegetation in green. \n",
    "\n",
    "`Figure 7` shows two false colour composites created from the Sentinel-2 MSI data captured on the same dates as before. The left image was taken before the fires on 1 August 2021 and the image on the right was taken after the fires were extinguished on 25 September 2021. In the false colour composite from September 2021, a large brown burn scar on Evia Island is clearly visible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part3_figure_3.png' width='80%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 7. False colour composites from Sentinel-2 Level 2A data showing pre-fire conditions on 1 August 2021 (left) and post-fire conditions on 25 September 2021 (right) in Greece. \n",
    "<br>Learn how these [false colour composites](./s2_dnbr.ipynb#s2_false_21) were made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### <a id='s2_dnbr'></a> Normalised Burn Ratio (NBR)\n",
    "In order to assess burn severity, we can calculate the Normalised Burn Ratio (NBR). As explained by <a href='https://un-spider.org/advisory-support/recommended-practices/recommended-practice-burn-severity/burn-severity-earth-engine' target='_blank'>UN SPIDER</a>, the NBR “uses near-infrared (NIR) and shortwave-infrared (SWIR) wavelengths. Healthy vegetation before the fire has very high NIR reflectance and a low SWIR response. In contrast, recently burned areas have a low reflectance in the NIR and high reflectance in the SWIR band.” The formula is `NBR = (NIR - SWIR) / (NIR + SWIR)` or using the MSI band numbers: `NBR = (B8A - B12) / (B8A + B12)`. \n",
    "\n",
    "Next, “The NBR is calculated for images before the fire (pre-fire NBR) and for images after the fire (post-fire NBR) and the post-fire image is subtracted from the pre-fire image to create the differenced (or delta) NBR (dNBR) image. dNBR can be used for burn severity assessment, as areas with higher dNBR values indicate more severe damage whereas areas with negative dNBR values might show increased vegetation productivity. dNBR can be classified according to burn severity ranges proposed by the United States Geological Survey (USGS).”\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part3_figure_4.png' width='90%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 8. The Differenced Normalised Burn Ratio (dNBR) image (right) is calculated from Sentinel-2 Level 2A data by subtracting the pre-fire NBR on 1 August 2021 (left) from the post-fire NBR on 25 September 2021 (middle). \n",
    "<br>Learn how the [NBR and dNBR images](./s2_dnbr.ipynb#s2_nbr_21) were made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "#### <a id='s2_burnseverity'></a> Burn Severity Map\n",
    "\n",
    "As shown in `Figure 9`, dNBR can be classified according to <a href='https://un-spider.org/advisory-support/recommended-practices/recommended-practice-burn-severity/in-detail/normalized-burn-ratio' target='_blank'>burn severity ranges</a> proposed by the USGS. The water bodies are also masked using the water mask from the Sentinel-2 data’s scene classification layer. The burn scar on Evia Island has been classified as having areas with both high and moderate-high severity burns.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<div align=\"center\"><img src='../img/med_part3_figure_5.png' width='60%'></img></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<i>Figure 9. Burn severity in Greece is classified using the dNBR image shown in Figure 8. The coastal water bodies are masked using the water mask from the Sentinel-2 data’s scene classification layer.\n",
    "<br>Learn how this [burn severity map](./s2_dnbr.ipynb#s2_burnseverity_21) was made.\n",
    "</i>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<p style=\"text-align:right;\">This project is licensed under the <a href=\"./LICENSE\">MIT License</a> and is developed under a Copernicus contract."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
