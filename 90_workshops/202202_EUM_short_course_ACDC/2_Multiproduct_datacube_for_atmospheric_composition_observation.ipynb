{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c17561e3",
   "metadata": {},
   "source": [
    "<img src='../img/EUMETSAT_Logo.png' alt='Logo EUMETSAT' align='right' width='30%'></img>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63910032",
   "metadata": {},
   "source": [
    "<br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96351728",
   "metadata": {},
   "source": [
    "<a href=\"../00_index.ipynb\"><< Index</a><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d48c0c9",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "<b>60 - DATA CUBE</b></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ecac7d15",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d88f88e",
   "metadata": {},
   "source": [
    "# NO<sub>2</sub> data time-series from different satellites using Data Tailor"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee55eb01",
   "metadata": {},
   "source": [
    "This is a Python-based training module presenting how to use the **EUMETSAT Atmospheric Composition Data Cube** (ACDC) plugin for the **EUMETSAT Data Tailor** to work with satellite-derived geophysical variables relevant for atmospheric monitoring.\n",
    "\n",
    "## Introduction\n",
    "\n",
    "The **Data Tailor ACDC plugin** simplifies the process to manipulate data and thus visualise the datasets on maps.\n",
    "Specifically, the plugin features the following functionalities:\n",
    "* it downloads the desired data from the reference website\n",
    "* it allows cropping a region of interest (ROI) \n",
    "* it allows defining a filter to save only the variables of interest (layer filter)\n",
    "* it allows defining the time coverage of the dataset (accordigly to the available input data)\n",
    "* it converts input products to NetCDF format compliant with CF-conventions and Climate Data Store ([CDS](https://cds.climate.copernicus.eu)) Common Data Model with coordinates dimensions\n",
    "* it allows time aggregation returning a single dataset\n",
    "* it estimates the output products size in advance.\n",
    "\n",
    "This notebook presents an example of how to obtain a Data Cube containing NO<sub>2</sub> data from different satellites. \n",
    "In particular, you will work with GOME-2 from Metop-B (in ASCII format) and TROPOMI Sentinel-5P data. Since input data is provided at different resolutions, you need to perform resampling to bring them to the same resolution and combine them in a single dataset.  \n",
    "Finally, you will produce and compare the Time-series of NO<sub>2</sub> variable from the two satellites on Darmstadt city (Germany).\n",
    "\n",
    "In this tutorial **you will learn how to use the following ACDC plugin features**:\n",
    "* define the configuration for each target product specifying:\n",
    "    * a time period\n",
    "    * a region of interest (ROI)\n",
    "    * time aggregation\n",
    "* invoke the Data Tailor interface\n",
    "* generate the output products in NetCDF format.\n",
    "\n",
    "With the output products, you will:\n",
    "* resample data to bring the two products at the same resolution (one of the two following options can be used):\n",
    "    * downsample TROPOMI data to the GOME-2 resolution\n",
    "    * upsample GOME-2 data to the TROPOMI resolution\n",
    "* combine data in a single dataset (once they have the same resolution)\n",
    "* select specific variables from the output dataset \n",
    "* produce a time-series of different variables.\n",
    "\n",
    "This notebook is designed for *medium-level* users with basic knowledge about Python and Atmospheric composition data."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf53eb9b",
   "metadata": {},
   "source": [
    "### Module outline:\n",
    "* [1 - Obtain NO<sub>2</sub> data using the *Atmospheric Composition Data Cube* plugin](#Obtain-NO2-data-using-the-Atmospheric-Composition-Data-Cube-plugin)\n",
    "* [2 - Combine data in a single output dataset](#Combine-data-in-a-single-output-dataset)\n",
    "* [3 - Visualise time-series of different NO<sub>2</sub> satellite products](#Visualise-time-series-of-different-NO2-satellite-products)\n",
    "* [4 - Save the data cube locally](#Save-the-data-cube-locally)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aaa8e292",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84d114b7",
   "metadata": {},
   "source": [
    "### Load required external libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a136fbd0",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "from pathlib import Path\n",
    "import xarray as xr\n",
    "\n",
    "from matplotlib import pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dcb5272c-e26b-4db6-9486-ff11b80d1630",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.environ['PATH'] = f\"/opt/conda/envs/acdc-training/bin/:{os.environ['PATH']}\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c3736214",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load auxiliary functions\n",
    "%run ../functions.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "813d6ac7",
   "metadata": {},
   "source": [
    "## Obtain NO<sub>2</sub> data using the *Atmospheric Composition Data Cube* plugin"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b82f98a7",
   "metadata": {},
   "source": [
    "To use the functionalities of the ACDC plugin, you only need to import the **Data Tailor API**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bd8b4dcd",
   "metadata": {},
   "outputs": [],
   "source": [
    "from epct import api"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc51432a",
   "metadata": {},
   "source": [
    "### Define the Data Tailor configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e8e19c9",
   "metadata": {},
   "source": [
    "This section contains the steps necessary to define valid Data Tailor configurations to obtain all the products you want to be included in your data cube.\n",
    "\n",
    "Let's start selecting the products for the Data Tailor interface. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79871c52",
   "metadata": {},
   "source": [
    "#### Select the satelllite products\n",
    "\n",
    "For this example, we want to include two different satellite data. Therefore, we define a \"product_id\" variable for each satellite data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c8acf7b8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input products for Data Cube\n",
    "product_id_A = 'GOME2_METOPB_NO2_ASCII'\n",
    "product_id_B = 'TROPOMI_S5P_NO2'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e177ecd",
   "metadata": {},
   "source": [
    "**Note**: to obtain the dictionary of supported products, see [0_Introduction_to_Data_Tailor_ACDC_plugin](0_Introduction_to_Data_Tailor_ACDC_plugin.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4185b170",
   "metadata": {},
   "source": [
    "#### Define the time coverage\n",
    "\n",
    "Let's visualise which are the time availability of the selected products:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c750553c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Discover the available temporal availability\n",
    "config = api.config()\n",
    "for product in [product_id_A, product_id_B]:\n",
    "    product_specs = config['backends_configurations']['epct_acdc']['INPUT_PRODUCTS'][product]\n",
    "    print(product)\n",
    "    print(\"periodicity: {}\".format(product_specs['periodicity']))\n",
    "    print(\"start-date: {}\".format(product_specs['start_date']))\n",
    "    if 'end_date' in product_specs:\n",
    "        print(\"stop-date: {}\".format(product_specs['end_date']))\n",
    "    print('\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d41ea9dc",
   "metadata": {},
   "source": [
    "As \"stop-time\" is not defined for the current data, it means the products continue to be recorded. \n",
    "\n",
    "You can select a time subset defining the following two variables:\n",
    "* *sensing_start*: a date equal to or later than the available \"start-date\"\n",
    "* *sensing_stop*: a date earlier than or equal to the available \"stop-date\", if any\n",
    "\n",
    "**Note**: dates must be defined in the following format: $$'<year><month><day>T000000Z'$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "93bd7b70",
   "metadata": {},
   "outputs": [],
   "source": [
    "sensing_start = '20180201T000000Z'\n",
    "sensing_stop  = '20210201T000000Z'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e900485e",
   "metadata": {},
   "source": [
    "**Note**: by default, if you don't specify this arguments in the customisation function, the plugin retrieves automatically all the available time stamps from the reference web provider. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f41d9b4d",
   "metadata": {},
   "source": [
    "#### (Optional) Define your geographical region of interest (ROI)\n",
    "\n",
    "Using the Data Tailor interface, you can define a region of interest to obtain a spatial subset of the original data.\n",
    "\n",
    "For this example, select the `Germany` region.\n",
    "\n",
    "To this purpose, you can either:\n",
    "* use pre-defined ROI selections \n",
    "* define your own selection bounds via a dictionary containing the key `NSWE` (for example, Germany corresponds to `roi = {'NSWE': [54.983104, 47.302488, 5.988658, 15.016996]}`). \n",
    "\n",
    "In this notebook, you will use the pre-defined filter, which can be invoked using the corresponding `id 'germany'`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8107f362",
   "metadata": {},
   "outputs": [],
   "source": [
    "roi = 'germany'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e6ada91d",
   "metadata": {},
   "source": [
    "**Note**: see how to list the available pre-defined filters in the [0_Introduction_to_Data_Tailor_ACDC_plugin](0_Introduction_to_Data_Tailor_ACDC_plugin.ipynb) notebook."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b502f72f",
   "metadata": {},
   "source": [
    "### Generate your Atmospheric Composition Data Cube\n",
    "\n",
    "To retrieve and customise the two different products, you have to invoke the *Data Tailor API* `run_chain` function two times (one for each product). \n",
    "\n",
    "This action will: \n",
    "* download data from the reference web provider\n",
    "* convert input data in CF-compliant homogenised NetCDF format and crop it in the region of interest\n",
    "* perform time aggregation\n",
    "* return the paths to the output products\n",
    "\n",
    "To invoke `run_chain` you need to specify:\n",
    "* an input file\n",
    "* a directory to store the outputs\n",
    "* the customisation configurations\n",
    "* the start-date and end-date of the period of interest\n",
    "\n",
    "**Note**: we don't need real input file because the data are retrieved from provider. So, let's start creating a dummy file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "90929bb1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a dummy file\n",
    "Path('dummy.txt').touch()\n",
    "product_paths = ['dummy.txt']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3f204e41",
   "metadata": {},
   "source": [
    "Let's define the output path:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e36cdf6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the output path\n",
    "target_dir='.'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b662de40",
   "metadata": {},
   "source": [
    "Now, you need to define the configuration as a dictionary. \n",
    "For this example, the dictionary must have the following keys:\n",
    "* *product* (mandatory) \n",
    "* *format* (mandatory): use \"*netcdf4_acdc*\" to specify the conversion to NetCDF obedient to CF-convention\n",
    "* *aggregation* (optional): if you want the output of each product to be a single dataset containing all time-stamps\n",
    "* *roi* (optional): to crop the data on a region of interest.\n",
    "\n",
    "**Note**: To customise the two different products, define the configuration dictionary and invoke the Data Tailor customisation function in a `for loop`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6b9adf26",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "output_products = []\n",
    "\n",
    "for product in [product_id_A, product_id_B]:\n",
    "    \n",
    "    # Define the chain_config dictionary with the current product\n",
    "    chain_config = {\n",
    "        \"product\": product,\n",
    "        \"format\": \"netcdf4_acdc\",\n",
    "        \"aggregation\": \"time\",\n",
    "        \"roi\": roi\n",
    "    }\n",
    "    #Invoke run_chain function\n",
    "    output_paths = api.run_chain(\n",
    "        product_paths, \n",
    "        chain_config=chain_config, \n",
    "        target_dir=target_dir, \n",
    "        sensing_start=sensing_start,\n",
    "        sensing_stop=sensing_stop,\n",
    "    )\n",
    "    output_products.extend(output_paths) "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "76ca7e2d",
   "metadata": {},
   "source": [
    "The variable `output_products` is a list containing the output dataset paths of the Data Tailor customisations.\n",
    "\n",
    "Let's visualise the content of the **output_products** variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d7bd870",
   "metadata": {},
   "outputs": [],
   "source": [
    "output_products"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e855dc3",
   "metadata": {},
   "source": [
    "As you can see, the output consists of two NetCDF files (one for each satellite). In the following, you can use the `output_products` variable to load the obtained dataset.\n",
    "\n",
    "**Note**: in the customisation process, data is automatically converted to the `International System of units` (SI). Therefore, `no2tmc` variable is converted from `1e13 molec cm-2` to `mol m-2`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae0d6644",
   "metadata": {},
   "source": [
    "## Combine data in a single output dataset\n",
    "\n",
    "The overall goal is to incorporate different Atmospheric composition products in a single dataset.\n",
    "However, the products are given in a different resolution: \n",
    "* GOME-2 Metop-B Level 3 Nitrogen Dioxide (NO2) has a resolution of 0.25 x 0.25 degrees\n",
    "* TROPOMI Tropospheric Nitrogen Dioxide (NO2) has a resolution of 0.125 x 0.125 degrees\n",
    "\n",
    "Therefore, the first step is to resample data to have all at the same resolution and be able to combine them in a single dataset.\n",
    "To bring data at the same resolution, you can either:\n",
    "* downsample TROPOMI (to bring it at the GOME-2 resolution)\n",
    "* upsample GOME-2 data (to bring it at the TROPOMI resolution)\n",
    "\n",
    "\n",
    "In the following, both methods are shown. However, you will concatenate the original GOME-2 data with the downsampled TROPOMI one as downsampling data is the more common choice. \n",
    "\n",
    "**Note**: to perform downsampling you should apply a smoothing method to avoid the aliasing problem but this process is out of scope for the present training module. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eb123886",
   "metadata": {},
   "source": [
    "### Downsampling\n",
    "\n",
    "To downsample the TROPOMI data to the GOME-2 data resolution, you can use the `CDO` `gridboxmean` function. It performs the mean of the selected grid boxes (defined as the number of pixels to combine).\n",
    "\n",
    "To invoke the function, you need to specify the:\n",
    "* the number of longitude pixels to be averaged\n",
    "* the number of latitude pixels to be averaged\n",
    "* the input file to be processed\n",
    "* the output file name\n",
    "\n",
    "Because the ratio between the two resolution is two (for both *lat* and *lon*), we need to average 2x2 pixels."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bb3eed54",
   "metadata": {},
   "outputs": [],
   "source": [
    "lon_pixels = 2\n",
    "lat_pixels = 2\n",
    "data_to_be_downsampled = output_products[1] # corresponds to TROPOMI data\n",
    "output_name = 'tropomi_downsampled.nc'\n",
    "\n",
    "# Invoke the CDO function\n",
    "cmd_string = f\"! cdo gridboxmean,{lon_pixels},{lat_pixels} {data_to_be_downsampled} {output_name}\"\n",
    "os.system(cmd_string)\n",
    "\n",
    "print('Executing... \\n', cmd_string)\n",
    "\n",
    "downsampled_tropomi = xr.open_dataset(output_name)\n",
    "downsampled_tropomi"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b6e11d84",
   "metadata": {},
   "outputs": [],
   "source": [
    "# open the GOME2 dataset with xarray\n",
    "gome2_data = xr.open_dataset(output_products[0])\n",
    "gome2_data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6212ed0",
   "metadata": {},
   "source": [
    "### (Optional) Upsampling\n",
    "\n",
    "To upsample GOME-2 data to the TROPOMI data resolution, you can use the `xarray` method `interp`.\n",
    "It requires to define:\n",
    "* the new coordinate arrays\n",
    "* the interpolation method\n",
    "\n",
    "To define the new coordinates, you can directly retrieve the arrays from the TROPOMI dataset. So, let's start loading the dataset with `xarray` and use the label-based method to retrieve the `lon` and `lat` coordinates to be passed to the interpolation function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "146c80d6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# open the TROPOMI dataset with xarray\n",
    "tropomi_data = xr.open_dataset(output_products[1])\n",
    "tropomi_data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5376ef4a",
   "metadata": {},
   "outputs": [],
   "source": [
    "# retrieve the upsampling coordinates from tropomi data\n",
    "upsampling_lat = tropomi_data.lat\n",
    "upsampling_lon = tropomi_data.lon"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "286e2615",
   "metadata": {},
   "source": [
    "Now that you have the lon and lat arrays to interpolate GOME-2 data, open the GOME2 dataset with `xarray` as well."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c5482bd1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# open the GOME2 dataset with xarray\n",
    "gome2_data = xr.open_dataset(output_products[0])\n",
    "gome2_data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5781b954",
   "metadata": {},
   "source": [
    "Use the `interp` method of `xarray` to perform upsampling."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2b7e666f",
   "metadata": {},
   "outputs": [],
   "source": [
    "gome2_resampled_data = gome2_data.interp(lat=upsampling_lat, lon=upsampling_lon, method='linear')\n",
    "gome2_resampled_data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ebbbcd07",
   "metadata": {},
   "source": [
    "### Assemble data from different satellites\n",
    "\n",
    "Once the two arrays have the same resolution, you can assemble them together to obtain a single dataset. As they present the same variable, let's concatenate the two products over a new dimension called `product`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dab73f7e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# concatenate data from different products using the downsampled TROPOMI data and original GOME-2 data\n",
    "no2_data = xr.concat([downsampled_tropomi, gome2_data], dim='product')\n",
    "\n",
    "# decomment the following line to use the upsampled GOME-2 data with TROPOMI original resolution instead\n",
    "# no2_data = xr.concat([tropomi_data, gome2_resampled_data], dim='product')\n",
    "\n",
    "no2_data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "77076488",
   "metadata": {},
   "source": [
    "The result is a three-dimensional `xarray.Dataset`, with the dimensions `time`, `latitude` and `longitude` already having proper coordinates."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc8a2914",
   "metadata": {},
   "source": [
    "## Visualise time-series of different NO<sub>2</sub> satellite products\n",
    "\n",
    "\n",
    "Now that you have your desired data stored and homogenised in a single dataset, you can easily produce a time-series of the NO<sub>2</sub> variable from the two platforms. \n",
    "\n",
    "Select the variables using the `xarray attribute style access` for reading variables and attributes. The outputs are `xarray.DataArray` objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15ad4aac",
   "metadata": {},
   "outputs": [],
   "source": [
    "# code to select total no2 \n",
    "no2tmc = no2_data.no2tmc\n",
    "tropomi = no2tmc.isel(product=0)\n",
    "gome2 = no2tmc.isel(product=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e3e4d1cd",
   "metadata": {},
   "source": [
    "Let us have a look at the time-series for individual point locations.\n",
    "\n",
    "We specify latitude and longitude coordinates for Darmstadt city and plot the two `xarray.DataArrays` (one for each satellite) as line plots using the plotting wrapper of Python's matplotlib.\n",
    "Using xarray's label based selection method `sel` you can select data based on name or value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d0a31cb3",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Latitude / Longitude coordinates for Darmstadt\n",
    "city1 = 'Darmstadt'\n",
    "lat1 = 49.875\n",
    "lon1 = 8.650"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3f364f76",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(20,5))\n",
    "\n",
    "\n",
    "tropomi_total = tropomi.sel(lat=lat1, lon=lon1, method='nearest')\n",
    "tropomi_total.plot.line(linestyle='dashed', color='blue', label='tropomi')\n",
    "\n",
    "gome2_total = gome2.sel(lat=lat1, lon=lon1, method='nearest')\n",
    "gome2_total.plot.line(linestyle='dashed',color='green', label='gome2 (downsampled)')\n",
    "\n",
    "plt.xticks(fontsize=16)\n",
    "plt.yticks(fontsize=16)\n",
    "plt.title(no2tmc.attrs['long_name'], fontsize=20, pad=20)\n",
    "plt.ylabel(no2tmc.units, fontsize=16)\n",
    "plt.xlabel('Date', fontsize=16)\n",
    "plt.legend(fontsize=16,loc=1)\n",
    "plt.show()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d81e58d",
   "metadata": {},
   "source": [
    "## Save the data cube locally\n",
    "\n",
    "You can download the data cube locally by executing the following cell:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f11d4f4d",
   "metadata": {},
   "outputs": [],
   "source": [
    "download_file = './NO2_data.nc'\n",
    "print ('Saving to: ', download_file)\n",
    "\n",
    "no2_data.to_netcdf(path=download_file)\n",
    "print ('Data saved on disk!')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee5ef769",
   "metadata": {},
   "source": [
    "**Note**: the dataset could be unavailable locally on your machine, but it has been indeed generated on the host machine."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c5c7c61",
   "metadata": {},
   "source": [
    "## Remove all files from the target directory\n",
    "\n",
    "To clean up the target directory at the end of the notebook, you just need to execute the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25260aaa",
   "metadata": {},
   "outputs": [],
   "source": [
    "files_to_remove = os.listdir(target_dir)\n",
    "\n",
    "for file in files_to_remove:\n",
    "    if file.endswith(\".nc\") or file.endswith(\".txt\"):\n",
    "        os.remove(os.path.join(target_dir, file))\n",
    "print(os.listdir(target_dir))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96c3ecd8",
   "metadata": {},
   "source": [
    "<a href=\"../00_index.ipynb\"><< Index</a><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68bdd875",
   "metadata": {},
   "source": [
    "<p><img src='../img/EUMETSAT_Logo.png' align='left' alt='Logo EUMETSAT' width='15%'></img></p>\n",
    "<br clear=left>\n",
    "<p style=\"text-align:left;\">This project is licensed under the <a href=\"../LICENSE\">MIT License</a> | \n",
    "<span style=\"float:right;\"><a href=\"https://gitlab.eumetsat.int/data-tailor/acdc-notebooks\">View on GitLab</a> | <a href=\"https://training.eumetsat.int/\">EUMETSAT Training</a></p>"
   ]
  }
 ],
 "metadata": {
  "description": "This notebook shows how to obtain a Data Cube containing NO2 data from different satellites: GOME-2 from Metop-B data (in ASCII format) and TROPOMI Sentinel-5P data. With the data collected through the EUMETSAT Data Tailor, long term trends are provided for the NO2 variable and displayed for a European city.",
  "image": "../img/EUMETSAT_Logo.png",
  "kernelspec": {
   "display_name": "acdc-training",
   "language": "python",
   "name": "acdc-training"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.12"
  },
  "tags": {
   "category": "data_cube",
   "domain": "atmosphere",
   "level": "L3",
   "satellite": [
    "Metop-B",
    "Sentinel-5P"
   ],
   "sensor": [
    "GOME-2",
    "TROPOMI"
   ],
   "variable": "troposphere_mole_content_of_nitrogen_dioxide"
  },
  "title": "Multiple NO2 regridded data for long-term trends with Data Tailor"
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
