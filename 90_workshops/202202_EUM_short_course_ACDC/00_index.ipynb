{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c17561e3",
   "metadata": {},
   "source": [
    "<img src='img/EUMETSAT_Logo.png' alt='Logo EU Copernicus\n",
    "EUMETSAT' align='right' width='40%'></img>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d88f88e",
   "metadata": {},
   "source": [
    "# Learning Tool for ACDC - Atmospheric Composition Data Cube"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee55eb01",
   "metadata": {},
   "source": [
    "This is the index of Python-based training modules presenting how to use the **EUMETSAT Atmospheric Composition Data Cube** (ACDC) plugin for the **EUMETSAT Data Tailor** to work with satellite-derived geophysical variables relevant for atmospheric monitoring.\n",
    "The training modules are all based on Jupyter notebooks, which present some case study examples.\n",
    "\n",
    "Using the ACDC plugin, you can obtain CF-compliant datasets without downloading input data in advance: this step is performed directly by the plugin selecting one of the supported products.\n",
    "\n",
    "The primary outcomes of this introduction page are:\n",
    "* to present the supported satellite Atmospheric Composition data and how to obtain the list of them using the Data Tailor API\n",
    "* to present the `estimate_cube_size` function to estimate the Data Cube size in advance\n",
    "* to present the EUMETSAT Data Tailor Atmospheric Composition Data Cube plugin notebooks series."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf53eb9b",
   "metadata": {},
   "source": [
    "### Module outline:\n",
    "* [1 - Atmospheric Composition supported Data](#Atmospheric-Composition-supported-Data)\n",
    "* [2 - Estimate the cube size](#Estimate-the-cube-size)\n",
    "* [3 - ACDC notebooks](#ACDC-notebooks)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aaa8e292",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84d114b7",
   "metadata": {},
   "source": [
    "### Load required ACDC modules"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b82f98a7",
   "metadata": {},
   "source": [
    "To use the functionalities of the ACDC plugin, you only need to import the **Data Tailor API**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "ace953b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "from epct import api"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c14170b",
   "metadata": {},
   "source": [
    "Once the module is loaded, you can verify that the ACDC plugin is corretly installed by quering the API ```info``` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "0312bb3d",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'customisations_log_dir': '/home/jovyan/.cache/epct/log',\n",
      " 'epct_version': '2.15.0',\n",
      " 'etc_dir': '/home/jovyan/.config/epct',\n",
      " 'executable_paths': {'epct_acdc': None},\n",
      " 'installed_plugins': ['epct-plugin-acdc, 0.3.0'],\n",
      " 'output_dir': '/',\n",
      " 'registered_backends': ['epct_acdc'],\n",
      " 'workspace_dir': '/home/jovyan/.cache/epct'}\n"
     ]
    }
   ],
   "source": [
    "import pprint\n",
    "pprint.pprint(api.info())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "51356c92",
   "metadata": {},
   "source": [
    "As you can see, the `epct-plugin-acdc` is correctly installed and `epct_acdc` appears in the list of registerd backend."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc51432a",
   "metadata": {},
   "source": [
    "## Atmospheric Composition supported Data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38cca913",
   "metadata": {},
   "source": [
    "The ACDC plugin supports the following **satellite** data:\n",
    "* **METOP-A/B/C GOME-2  AAI** data \n",
    "    * *title*: GOME-2 Metop-A/B/C Level 3 Absorbing Aerosol Index (AAI)\n",
    "    * *product_id*: `GOME2_METOPA_AAI`, `GOME2_METOPB_AAI`, `GOME2_METOPC_AAI`\n",
    "* **METOP-A/B GOME-2 Level 3 HCHO** data \n",
    "    * *title*: GOME Metop-A/B Level 3 Tropospheric Formaldehyde (HCHO)\n",
    "    * *product_id*: `GOME2_METOPA_HCHO`, `GOME2_METOPB_HCHO`\n",
    "* **METOP-A/B GOME-2 Level 3 NO2** data \n",
    "    * *title*: GOME-2 Metop-A/B Level 3 Nitrogen Dioxide (NO2)\n",
    "    * *product_id*: `GOME2_METOPA_NO2`, `GOME2_METOPB_NO2`\n",
    "* **METOP-B GOME-2 NO2 ASCII** data\n",
    "    * *title*: GOME-2 Metop-B Level 3 Nitrogen Dioxide (NO2) - ASCII format\n",
    "    * *product_id*: `GOME2_METOPB_NO2_ASCII`\n",
    "* **METOP-A/B/C IASI Level 3 CO** data\n",
    "    * *title*: IASI Metop-A/B/C Level 3 Carbon monoxide (CO)\n",
    "    * *product_id*: `IASIL3_METOPA_CO`, `IASIL3_METOPB_CO`, `IASIL3_METOPC_CO`\n",
    "* **METOP-A/B/C IASI Level 3 HCHO** data\n",
    "    * *title*: IASI Metop-A/B/C Level 3 Formic acid (HCOOH)\n",
    "    * *product_id*: `IASIL3_METOPA_HCOOH`, `IASIL3_METOPB_HCOOH`, `IASIL3_METOPC_HCOOH`\n",
    "* **METOP-A/B/C IASI Level 3 NH3** data\n",
    "    * *title*: IASI Metop-A/B/C Level 3 Ammonia (NH3)\n",
    "    * *product_id*: `IASIL3_METOPA_NH3`, `IASIL3_METOPB_NH3`, `IASIL3_METOPC_NH3`\n",
    "* **Copernicus Sentinel-5P TROPOMI Level 2** data\n",
    "    * *title*: TROPOMI Tropospheric Nitrogen Dioxide (NO2) v1.0\n",
    "    * *product_id*: `TROPOMI_S5P_NO2`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4bccf7c",
   "metadata": {},
   "source": [
    "To discover which **satellite** data is supported by the **Data Tailor ACDC plugin**, you can also use the API `read` function specifying the `epct_acdc` backend, as shown below.\n",
    "\n",
    "Additionally, you can visualise the website from which the data is retrieved using the API function `config`.\n",
    "\n",
    "Execute the cell below to obtain the list of supported products."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "4645e4d9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "List of supported products:\n",
      "\n",
      "GOME-2 Metop-A Level 3 Absorbing Aerosol Index (AAI): \n",
      "        product_id = GOME2_METOPA_AAI\n",
      "        url = https://www.temis.nl/airpollution/absaai/#GOME2_AAI\n",
      "GOME Metop-A Level 3 Tropospheric Formaldehyde (HCHO): \n",
      "        product_id = GOME2_METOPA_HCHO\n",
      "        url = https://h2co.aeronomie.be/\n",
      "GOME-2 Metop-A Level 3 Nitrogen Dioxide (NO2): \n",
      "        product_id = GOME2_METOPA_NO2\n",
      "        url = https://acsaf.org/datarecords/no2_h2o_tcdr.php\n",
      "GOME-2 Metop-B Level 3 Absorbing Aerosol Index (AAI): \n",
      "        product_id = GOME2_METOPB_AAI\n",
      "        url = https://www.temis.nl/airpollution/absaai/#GOME2_AAI\n",
      "GOME Metop-B Level 3 Tropospheric Formaldehyde (HCHO): \n",
      "        product_id = GOME2_METOPB_HCHO\n",
      "        url = https://h2co.aeronomie.be/\n",
      "GOME-2 Metop-B Level 3 Nitrogen Dioxide (NO2): \n",
      "        product_id = GOME2_METOPB_NO2\n",
      "        url = https://acsaf.org/datarecords/no2_h2o_tcdr.php\n",
      "GOME-2 Metop-B Level 3 Nitrogen Dioxide (NO2) - ASCII format: \n",
      "        product_id = GOME2_METOPB_NO2_ASCII\n",
      "        url = https://www.temis.nl/airpollution/no2col/no2monthgome2b.php\n",
      "GOME-2 Metop-C Level 3 Absorbing Aerosol Index (AAI): \n",
      "        product_id = GOME2_METOPC_AAI\n",
      "        url = https://www.temis.nl/airpollution/absaai/#GOME2_AAI\n",
      "IASI Metop-A Level 3 Carbon monoxide (CO): \n",
      "        product_id = IASIL3_METOPA_CO\n",
      "        url = https://iasi.aeris-data.fr/CO_IASI_A_L3_data/\n",
      "IASI Metop-A Level 3 Formic acid (HCOOH): \n",
      "        product_id = IASIL3_METOPA_HCOOH\n",
      "        url = https://iasi.aeris-data.fr/HCOOH_IASI_A_L3_data/\n",
      "IASI Metop-A Level 3 Ammonia (NH3): \n",
      "        product_id = IASIL3_METOPA_NH3\n",
      "        url = https://iasi.aeris-data.fr/nh3_iasi_a_l3_data/\n",
      "IASI Metop-B Level 3 Carbon monoxide (CO): \n",
      "        product_id = IASIL3_METOPB_CO\n",
      "        url = https://iasi.aeris-data.fr/CO_IASI_B_L3_data/\n",
      "IASI Metop-B Level 3 Formic acid (HCOOH): \n",
      "        product_id = IASIL3_METOPB_HCOOH\n",
      "        url = https://iasi.aeris-data.fr/HCOOH_IASI_B_L3_data/\n",
      "IASI Metop-B Level 3 Ammonia (NH3): \n",
      "        product_id = IASIL3_METOPB_NH3\n",
      "        url = https://iasi.aeris-data.fr/nh3_iasi_b_l3_data/\n",
      "IASI Metop-C Level 3 Carbon monoxide (CO): \n",
      "        product_id = IASIL3_METOPC_CO\n",
      "        url = https://iasi.aeris-data.fr/CO_IASI_C_L3_data/\n",
      "IASI Metop-C Level 3 Formic acid (HCOOH): \n",
      "        product_id = IASIL3_METOPC_HCOOH\n",
      "        url = https://iasi.aeris-data.fr/HCOOH_IASI_C_L3_data/\n",
      "IASI Metop-C Level 3 Ammonia (NH3): \n",
      "        product_id = IASIL3_METOPC_NH3\n",
      "        url = https://iasi.aeris-data.fr/nh3_iasi_c_l3_data/\n",
      "TROPOMI Tropospheric Nitrogen Dioxide (NO2) v1.0: \n",
      "        product_id = TROPOMI_S5P_NO2\n",
      "        url = https://www.temis.nl/airpollution/no2col/no2month_tropomi.php\n"
     ]
    }
   ],
   "source": [
    "# Define a printing function for the products dictionary\n",
    "def print_supported_products(dct, config):\n",
    "    print(\"List of supported products:\\n\")\n",
    "    for key, value in dct.items(): \n",
    "        url = config['backends_configurations']['epct_acdc']['INPUT_PRODUCTS'][key]['attributes']\n",
    "        print(\"{}: \".format(value[\"name\"]))\n",
    "        print(\"        product_id = {}\".format(key))\n",
    "        print(\"        url = {}\".format(url['product_url']))\n",
    "        \n",
    "# Obtain the list of supported products and the web site from which each of them is downloaded\n",
    "products = api.read(\"products\", backend=\"epct_acdc\")\n",
    "config = api.config()\n",
    "\n",
    "# Display the list calling the above printing function\n",
    "print_supported_products(products, config)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4185b170",
   "metadata": {},
   "source": [
    "In the Data Tailor, the products are identified by means of the *product_id*."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25089a90",
   "metadata": {},
   "source": [
    "## Estimate the cube size\n",
    "\n",
    "With the plugin, we provide the possibility to estimate the data cube size before you start retrieving and customising the data. This preview is helpful to know in advance the size of data you will download locally.\n",
    "\n",
    "\n",
    "To use the `estimate_cube_size` function, you need to import the `acdc` module:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "b2ebeb09",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Import the acdc module\n",
    "from epct_plugin_acdc import acdc"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "61a616e3",
   "metadata": {},
   "source": [
    "Herein, we show the function interface to help you identify the arguments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "f5eee1db",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Help on function estimate_cube_size in module epct_plugin_acdc.acdc:\n",
      "\n",
      "estimate_cube_size(product, sensing_start, sensing_stop, roi=None, band_filter=None)\n",
      "    Estimate the size of the required data cube in Mb.\n",
      "    \n",
      "    :param str product: product-ID (e.g. \"IASIL3_METOPB_CO\")\n",
      "    :param sensing_start: start date time\n",
      "    :param sensing_stop: stop date time\n",
      "    :param dict roi: (optional) by default is set to None, i.e. it takes into account the original spatial extension\n",
      "    :param dict band_filter: (optional) by default is set to None, i.e. it takes into account all available variables\n",
      "    :return float full_cube_size: the cube size in Mb\n",
      "\n"
     ]
    }
   ],
   "source": [
    "help(acdc.estimate_cube_size)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "13b70d23",
   "metadata": {},
   "source": [
    "The arguments are here summarised:\n",
    "* `product`: is the *product_id* of the selected record\n",
    "* `sensing_start`: the start date of the cube time coverage\n",
    "* `sensing_stop`: the stop date of the cube time coverage\n",
    "* `roi`: (optional) if you want to perform a spatial subset of the data, you have to specify the latitude and longitude bounds (North, South, West, East) in a dictionary containing the key `NSWE`\n",
    "* `band_filter`: (optional) if you want to perform a variable filter (one or more variables), you have to define it via a dictionary containing the key `bands`.\n",
    "\n",
    "An example of a data cube size estimate is given for the `IASI Metop-A Level 3 Carbon monoxide (CO)` record. Therefore, in the following, you will see how to retrieve and define the customisation specifications which determine the data cube size.\n",
    "These parameters define part of the Data Tailor customisation configuration. To see an example of Data Tailor configuration, see [0_Introduction_to_Data_Tailor_ACDC_plugin](60_data_cube/0_Introduction_to_Data_Tailor_ACDC_plugin.ipynb).\n",
    "\n",
    "**Limitation**: Note that, the Data Tailor API allows you to use pre-defined filters for both ROI extraction and layer filtering. However, the `estimate_cube_size` function doesn't support such a definition. Therefore, you have to specify them via the `api.read` function as shown in [0_Introduction_to_Data_Tailor_ACDC_plugin](60_data_cube/0_Introduction_to_Data_Tailor_ACDC_plugin.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6fd0308c",
   "metadata": {},
   "source": [
    "### Select the satellite products\n",
    "\n",
    "From the list of supported products presented above, select one (or more) products by means of their `product-id`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "cb6bd9e1",
   "metadata": {},
   "outputs": [],
   "source": [
    "product_id = 'IASIL3_METOPA_CO'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ff26410",
   "metadata": {},
   "source": [
    "### Define the time coverage\n",
    "\n",
    "To know the **time extent** of this product, you can execute the cell below. \n",
    "If \"stop-time\" is not defined for the current data, it means the product continues to be recorded.  \n",
    "\n",
    "Verify that the data is available for the dates  `01 February 2018`   to   `01 July 2021`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "d69cc3b7",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "IASIL3_METOPA_CO\n",
      "periodicity: monthly\n",
      "start-date: 20071001\n"
     ]
    }
   ],
   "source": [
    "# Discover the available temporal availability\n",
    "config = api.config()\n",
    "\n",
    "product_specs = config['backends_configurations']['epct_acdc']['INPUT_PRODUCTS'][product_id]\n",
    "print(product_id)\n",
    "print(\"periodicity: {}\".format(product_specs['periodicity']))\n",
    "print(\"start-date: {}\".format(product_specs['start_date']))\n",
    "if 'end_date' in product_specs:\n",
    "    print(\"stop-date: {}\".format(product_specs['end_date']))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "78f664d1",
   "metadata": {},
   "source": [
    "Based on that, you can define a time subset for your data cube specifying the following variables:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "2695374c",
   "metadata": {},
   "outputs": [],
   "source": [
    "sensing_start = '20180201T000000Z'\n",
    "sensing_stop = '20210701T000000Z' "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2fd8cb4e",
   "metadata": {},
   "source": [
    "### (Optional) Define your geographical region of interest (ROI)\n",
    "\n",
    "To crop a **region of interest**, you have to specify the latitude and longitude bounds (North, South, West, East) in a dictionary containing the key `NSWE`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "df5e07be",
   "metadata": {},
   "outputs": [],
   "source": [
    "roi = {'NSWE': [72, 35, -10, 33]} #Central Europe"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40fd3d20",
   "metadata": {},
   "source": [
    "### (Optional) Select the layer(s) of interest\n",
    "\n",
    "As for the roi extension, also the **band filtering** is defined in a dictionary variable which must contain the `bands`key."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "127dd402",
   "metadata": {},
   "outputs": [],
   "source": [
    "band_filter = {'bands': ['co_day', 'error_day']}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8fc5bb66",
   "metadata": {},
   "source": [
    "### Invoke the auxiliary function for the size estimate\n",
    "\n",
    "Now that all the required inputs are defined, estimate the Data Cube size:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "6c9db0bc",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Cube size is  1.13 Mb.\n"
     ]
    }
   ],
   "source": [
    "cube_size = acdc.estimate_cube_size(product_id, sensing_start, sensing_stop, roi=roi, band_filter=band_filter)\n",
    "print('Cube size is {:5.2f} Mb.'.format(cube_size))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc8a2914",
   "metadata": {},
   "source": [
    "## ACDC notebooks\n",
    "\n",
    "The following notebooks contain examples of data operations using the Data Tailor Atmospheric Composition Data Cube plugin.\n",
    "\n",
    "* [0_Introduction_to_Data_Tailor_ACDC_plugin](60_data_cube/0_Introduction_to_Data_Tailor_ACDC_plugin.ipynb)\n",
    "    * keywords: #Data-Cube, #ROI, #layer-filtering #Data-Tailor #CF-compliant\n",
    "* [1_Multivariable_datacube_for_atmospheric_composition_observation](60_data_cube/1_Multivariable_datacube_for_atmospheric_composition_observation.ipynb)\n",
    "    * keywords: #Air-Pollution, #IASI, #CO-HCOOH-HN3, #Data-Cube \n",
    "* [2_Multiproduct_datacube_for_atmospheric_composition_observation](60_data_cube/2_Multiproduct_datacube_for_atmospheric_composition_observation.ipynb)\n",
    "    * keywords: #Long-term-trend, #Sentinel-5P, #Metop-B, #NO2 \n",
    "* [3_Multiplatform_datacube_for_atmospheric_composition_observation](60_data_cube/3_Multiplatform_datacube_for_atmospheric_composition_observation.ipynb)\n",
    "    * keywords: #Absorbing-Aerosol-Index, #GOME2, #Wildfire, #Data-Cube \n",
    "* [4_Multisensor_multiplatform_datacube_for_atmospheric_composition_observation](60_data_cube/4_Multisensor_multiplatform_datacube_for_atmospheric_composition_observation.ipynb)\n",
    "    * keywords: #Air-Pollution, #NO2, #GOME2-L3"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9903d174",
   "metadata": {},
   "source": [
    "<p><img src='img/EUMETSAT_Logo.png' align='left' alt='Logo EUMETSAT' width='15%'></img></p>\n",
    "<br clear=left>\n",
    "<p style=\"text-align:left;\">This project is licensed under the <a href=\"../LICENSE\">MIT License</a> | \n",
    "<span style=\"float:right;\"><a href=\"https://gitlab.eumetsat.int/data-tailor/acdc-notebooks\">View on GitLab</a> | <a href=\"https://training.eumetsat.int/\">EUMETSAT Training</a></p>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "epct",
   "language": "python",
   "name": "epct"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.12"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
