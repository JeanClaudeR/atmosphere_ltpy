{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "c17561e3",
   "metadata": {},
   "source": [
    "<img src='../img/EUMETSAT_Logo.png' alt='Logo EUMETSAT' align='right' width='30%'></img>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63910032",
   "metadata": {},
   "source": [
    "<br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96351728",
   "metadata": {},
   "source": [
    "<a href=\"../00_index.ipynb\"><< Index</a><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d48c0c9",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "<b>20 - DATA CUBE</b></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ecac7d15",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d88f88e",
   "metadata": {},
   "source": [
    "# Create an Atmospheric Composition Data Cube of IASI data time-series using Data Tailor"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ee55eb01",
   "metadata": {},
   "source": [
    "This is a Python-based training module presenting how to use the **EUMETSAT Atmospheric Composition Data Cube** (ACDC) plugin for the **EUMETSAT Data Tailor** to work with satellite-derived geophysical variables relevant for atmospheric monitoring.\n",
    "\n",
    "## Introduction\n",
    "\n",
    "The **Data Tailor ACDC plugin** simplifies the process to manipulate data and thus visualise the datasets on maps.\n",
    "Specifically, the plugin features the following functionalities:\n",
    "* it downloads the desired data from the reference website\n",
    "* it allows cropping a region of interest (ROI) \n",
    "* it allows defining a filter to save only the variables of interest (layer filter)\n",
    "* it allows defining the time coverage of the dataset (accordigly to the available input data)\n",
    "* it converts input products to NetCDF format compliant with CF-conventions and Climate Data Store ([CDS](https://cds.climate.copernicus.eu)) Common Data Model with coordinates dimensions\n",
    "* it allows time aggregation returning a single dataset\n",
    "* it estimates the output products size in advance.\n",
    "\n",
    "This notebook presents an example on how to obtain a Data Cube containing Atmospheric Composition variables such as CO, HCOOH, and NH<sub>3</sub>. For this example, *Metop-A Level 3 data from IASI* is used.\n",
    "\n",
    "\n",
    "In this tutorial **you will learn how to use the following ACDC plugin features**:\n",
    "* define the configurations to obtain the three IASI Metop-A data variables specifying:\n",
    "    * a time coverage subset\n",
    "    * a layer filter for each product\n",
    "* invoke the Data Tailor interface\n",
    "* generate the output products in NetCDF format.\n",
    "\n",
    "With the output products, you will:\n",
    "* combine data in a single dataset\n",
    "* produce a time-series for each variable\n",
    "* visualise data on 2D-Map\n",
    "* save your data cube locally.\n",
    "\n",
    "This notebook is designed for *medium-level* users with basic knowledge about Python and Atmospheric composition data."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf53eb9b",
   "metadata": {},
   "source": [
    "### Module outline:\n",
    "* [1 - Obtain IASI data using the *Atmospheric Composition Data Cube* plugin](#Obtain-IASI-data-using-the-Atmospheric-Composition-Data-Cube-plugin)\n",
    "* [2 - Combine data in a single output dataset](#Combine-data-in-a-single-output-dataset)\n",
    "* [3 - Visualise time-series of IASI products](#Visualise-time-series-of-IASI-products)\n",
    "* [4 - Save the data cube locally](#Save-the-data-cube-locally) "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aaa8e292",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84d114b7",
   "metadata": {},
   "source": [
    "### Load required external libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a136fbd0",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "from pathlib import Path\n",
    "import xarray as xr\n",
    "\n",
    "from matplotlib import pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0cf27e4e-ded5-438e-8061-a88107ec81db",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.environ['PATH'] = f\"/opt/conda/envs/acdc-training/bin/:{os.environ['PATH']}\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c3736214",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load auxiliary functions\n",
    "%run ../functions.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "813d6ac7",
   "metadata": {},
   "source": [
    "## Obtain IASI data using the *Atmospheric Composition Data Cube* plugin"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b82f98a7",
   "metadata": {},
   "source": [
    "To use the functionalities of the ACDC plugin, you only need to import the **Data Tailor API**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bd8b4dcd",
   "metadata": {},
   "outputs": [],
   "source": [
    "from epct import api"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bc51432a",
   "metadata": {},
   "source": [
    "### Define the Data Tailor configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e8e19c9",
   "metadata": {},
   "source": [
    "This section contains the steps necessary to define valid Data Tailor configurations to obtain all the products you want to be included in your data cube.\n",
    "\n",
    "Let's start selecting the IASI products for the Data Tailor interface. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "79871c52",
   "metadata": {},
   "source": [
    "#### Select the satelllite products\n",
    "\n",
    "For this example, we want to include three different satellite data. Therefore, we define a \"product_id\" variable for each satellite data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c8acf7b8",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input products for Data Cube\n",
    "product_id_A = 'IASIL3_METOPA_CO'\n",
    "product_id_B = 'IASIL3_METOPA_HCOOH'\n",
    "product_id_C = 'IASIL3_METOPA_NH3'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e369cc4b",
   "metadata": {},
   "source": [
    "**Note**: to obtain the dictionary of supported products, see [0_Introduction_to_Data_Tailor_ACDC_plugin](0_Introduction_to_Data_Tailor_ACDC_plugin.ipynb)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4185b170",
   "metadata": {},
   "source": [
    "#### Define the time coverage\n",
    "\n",
    "Let's visualise which are the time availability of the selected products:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c750553c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Discover the available temporal availability\n",
    "config = api.config()\n",
    "for product in [product_id_A, product_id_B, product_id_C]:\n",
    "    product_specs = config['backends_configurations']['epct_acdc']['INPUT_PRODUCTS'][product]\n",
    "    print(product)\n",
    "    print(\"periodicity: {}\".format(product_specs['periodicity']))\n",
    "    print(\"start-date: {}\".format(product_specs['start_date']))\n",
    "    if 'end_date' in product_specs:\n",
    "        print(\"stop-date: {}\".format(product_specs['end_date']))\n",
    "    print('\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d41ea9dc",
   "metadata": {},
   "source": [
    "The products for which \"stop-time\" is not defined, it means the products continue to be recorded. \n",
    "\n",
    "You can select a time subset defining the following two variables:\n",
    "* *sensing_start*: a date equal to or later than the available \"start-date\"\n",
    "* *sensing_stop*: a date earlier than or equal to the available \"stop-date\", if any\n",
    "\n",
    "**Note**: dates must be defined in the following format: $$'<year><month><day>T000000Z'$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "93bd7b70",
   "metadata": {},
   "outputs": [],
   "source": [
    "sensing_start = '20170101T000000Z'\n",
    "sensing_stop  = '20200101T000000Z'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e900485e",
   "metadata": {},
   "source": [
    "**Note**: by default, if you don't specify this arguments in the customisation function, the plugin retrieves automatically all the available time stamps from the reference web provider. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f3142a50",
   "metadata": {},
   "source": [
    "#### (Optional) Select the layer(s) of interest\n",
    "\n",
    "You can visualise the available layers for each product using the Data Tailor API `read` function. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1ae7bf93",
   "metadata": {},
   "outputs": [],
   "source": [
    "for product in [product_id_A, product_id_B, product_id_C]:\n",
    "    # Obtain the list of available layers             \n",
    "    info = api.read(f\"products/{product}\")\n",
    "    print('\\n{} layers: '.format(product))\n",
    "    layers=info['bands']\n",
    "    for key, value in layers.items():\n",
    "        print(\"{}) {}: {}\".format(value['number'], key, value['name']))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b1253226",
   "metadata": {},
   "source": [
    "Based on the above output, select the variables of interest for each product using a dictionary containing the key `bands`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5883a85f",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input product layer selection\n",
    "layer_filter_A = {'bands': ['co_day']}\n",
    "layer_filter_B = {'bands': ['hcooh']}\n",
    "layer_filter_C = {'bands': ['nh3_day']}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a20ac4d0",
   "metadata": {},
   "source": [
    "**Note**: the Data Tailor provides also pre-defined filters tailored to each product. To see how to visualise available filters and use them, see [0_Introduction_to_Data_Tailor_ACDC_plugin](0_Introduction_to_Data_Tailor_ACDC_plugin.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b502f72f",
   "metadata": {},
   "source": [
    "### Generate your Atmospheric Composition Data Cube\n",
    "\n",
    "To retrieve and customise the three different products, you have to invoke the *Data Tailor API* `run_chain` function three times (one for each product). \n",
    "\n",
    "This action will: \n",
    "* download data from the reference web provider\n",
    "* convert input data in CF-compliant homogenised NetCDF format\n",
    "* perform time aggregation\n",
    "* return the paths to the output products.\n",
    "\n",
    "To invoke `run_chain` you need to specify:\n",
    "* an input file\n",
    "* a directory to store the outputs\n",
    "* the customisation configuration\n",
    "* the start-date and end-date of the period of interest.\n",
    "\n",
    "**Note**: we don't need real input file because the data are retrieved from provider. So, let's start creating a dummy file."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "90929bb1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a dummy file\n",
    "Path('dummy.txt').touch()\n",
    "product_paths = ['dummy.txt']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aacf5671",
   "metadata": {},
   "source": [
    "Let's define the output path:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4e36cdf6",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the output path\n",
    "target_dir='.'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b662de40",
   "metadata": {},
   "source": [
    "Now, you need to define each configuration as a dictionary.\n",
    "\n",
    "For this example, the dictionary must have the following keys:\n",
    "* *product* (mandatory) \n",
    "* *format* (mandatory): use \"*netcdf4_acdc*\" to specify the conversion to NetCDF obedient to CF-convention\n",
    "* *aggregation* (optional): if you want the output to be a single dataset containing all product time-stamps\n",
    "* *filter* (optional): to filter only variables of interest\n",
    "\n",
    "**Note**: To retrieve the three different products, define the configuration dictionary and invoke the customisation function in a `for loop`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6b9adf26",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "output_products = []\n",
    "\n",
    "for product, layer in [(product_id_A, layer_filter_A), (product_id_B,layer_filter_B), (product_id_C, layer_filter_C)]:\n",
    "    \n",
    "    # Define the chain_config dictionary with the current product\n",
    "    chain_config = {\n",
    "        \"product\": product,\n",
    "        \"format\": \"netcdf4_acdc\",\n",
    "        \"aggregation\": \"time\",\n",
    "        \"filter\": layer\n",
    "    }\n",
    "    #Invoke run_chain function\n",
    "    output_paths = api.run_chain(\n",
    "        product_paths, \n",
    "        chain_config=chain_config, \n",
    "        target_dir=target_dir, \n",
    "        sensing_start=sensing_start,\n",
    "        sensing_stop=sensing_stop,\n",
    "    )\n",
    "    output_products.extend(output_paths) "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "76ca7e2d",
   "metadata": {},
   "source": [
    "The variable `output_products` is a list containing the output dataset paths of the Data Tailor customisations.\n",
    "\n",
    "Let's visualise the content of the **output_products** variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51f9c471",
   "metadata": {},
   "outputs": [],
   "source": [
    "output_products"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8e855dc3",
   "metadata": {},
   "source": [
    "As you can see, the output consists of three NetCDF files containing all required time-stamp (one for each satellite). In the following, you can use the `output_products` variable to load the obtained dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae0d6644",
   "metadata": {},
   "source": [
    "## Combine data in a single output dataset\n",
    "\n",
    "The overall goal is to incorporate different Atmospheric composition variables in a single dataset.\n",
    "\n",
    "To this purpose, you can use the `open_mfdataset` function from the `xarray` library.\n",
    "\n",
    "The result is a three-dimensional `xarray.Dataset`, with the dimensions `time`, `latitude` and `longitude` already having proper coordinates. The dataset contains all the requested variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7e6a3f17",
   "metadata": {},
   "outputs": [],
   "source": [
    "output_data = xr.open_mfdataset([output_products[0], output_products[1], output_products[2]], data_vars='all')\n",
    "output_data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fc8a2914",
   "metadata": {},
   "source": [
    "## Visualise time-series of IASI products\n",
    "\n",
    "\n",
    "Now that you have your desired data stored and homogenised in a single dataset, you can easily produce a time-series of each variable. \n",
    "\n",
    "To recall you the variable names, use the `xarray`'s label based method `keys`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5c483361",
   "metadata": {},
   "outputs": [],
   "source": [
    "vars_list = list(output_data.keys())\n",
    "vars_list"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8abac3c",
   "metadata": {},
   "source": [
    "Based on the list above, select the variables using the `xarray attribute style access` for reading variables and attributes. The outputs are `xarray.DataArray` objects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "15ad4aac",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select variables\n",
    "coamcd = output_data.coamcd \n",
    "hcoohamc = output_data.hcoohamc\n",
    "nh3amcd = output_data.nh3amcd"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8f9fb63b",
   "metadata": {},
   "source": [
    "Finally, let's have a look at the time-series for individual point locations.\n",
    "\n",
    "We specify latitude and longitude coordinates for two cities and plot the two `xarray.DataArrays` as line plots using the plotting wrapper of Python's matplotlib."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6096e16c",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Latitude / Longitude coordinates for Darmstadt\n",
    "city1 = 'Darmstadt'\n",
    "lat1 = 49.875\n",
    "lon1 = 8.650\n",
    "\n",
    "# Latitude / Longitude coordinates for Accra (Capital of Ghana)\n",
    "city2 = 'Accra'\n",
    "lat2 = 5.6037\n",
    "lon2 = 0.187"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "891ea9d9",
   "metadata": {},
   "source": [
    "Using xarray's label based selection method `sel` we select data based on name or value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a0ff9adf",
   "metadata": {},
   "outputs": [],
   "source": [
    "from pandas.plotting import register_matplotlib_converters\n",
    "register_matplotlib_converters()\n",
    "\n",
    "fig = plt.figure(figsize=(20,5))\n",
    "\n",
    "\n",
    "city1_total = coamcd.sel(lat=lat1, lon=lon1, method='nearest')\n",
    "city1_total.plot.line(color='blue', linestyle='dashed', label=city1)\n",
    "\n",
    "city2_total = coamcd.sel(lat=lat2, lon=lon2, method='nearest')\n",
    "city2_total.plot.line(linestyle='dashed',color='green', label=city2)\n",
    "\n",
    "plt.xticks(fontsize=16)\n",
    "plt.yticks(fontsize=16)\n",
    "plt.title(\"Carbon Monoxide - over time\", fontsize=20, pad=20)\n",
    "plt.xlabel('Date', fontsize=16)\n",
    "plt.legend(fontsize=16,loc=1)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a2dfafd3",
   "metadata": {},
   "source": [
    "Now, let's plot the `HCOOH` and `NH3` time-series for Darmstadt city."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6abad08c",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(20,5))\n",
    "\n",
    "city1_hcooh = hcoohamc.sel(lat=lat1, lon=lon1, method='nearest')\n",
    "city1_hcooh.plot.line(color='purple', linestyle='dashed', label='HCOOH')\n",
    "city2_hcooh = hcoohamc.sel(lat=lat2, lon=lon2, method='nearest')\n",
    "city2_hcooh.plot.line(color='orange', linestyle='dashed', label='NH3')\n",
    "\n",
    "plt.xticks(fontsize=16)\n",
    "plt.yticks(fontsize=16)\n",
    "plt.title('Darmstadt atmospheric components' + ' - over time', fontsize=20, pad=20)\n",
    "# plt.ylabel(str(conversion_factor) + ' ' + no2tmc.units, fontsize=16)\n",
    "plt.xlabel('Date', fontsize=16)\n",
    "plt.legend(fontsize=16,loc=1)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9ca78abc",
   "metadata": {},
   "source": [
    "With the output dataset, you can visualise a `2D Map` for a selected time stamp. For example, visualise the `CO` content in February 2019."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c8c99eeb",
   "metadata": {},
   "outputs": [],
   "source": [
    "coamcd = coamcd.transpose(..., \"lat\", \"lon\")\n",
    "month=25\n",
    "visualize_pcolormesh(data_array=coamcd.isel(time=month),\n",
    "                     longitude=coamcd.lon, \n",
    "                     latitude=coamcd.lat,\n",
    "                     projection=ccrs.PlateCarree(), \n",
    "                     color_scale='afmhot_r', \n",
    "                     unit= coamcd.units,\n",
    "                     long_name=coamcd.long_name + ': ' + str(coamcd.time[month].dt.strftime('%Y-%m').data), \n",
    "                     vmin=0, \n",
    "                     vmax=1e-01, \n",
    "                     set_global=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9600c589",
   "metadata": {},
   "source": [
    "## Save the data cube locally"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3d7b3384",
   "metadata": {},
   "source": [
    "You can download the data cube locally by executing the following cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3af66b3f",
   "metadata": {},
   "outputs": [],
   "source": [
    "download_file = './IASI_data.nc'\n",
    "print ('Saving to: ', download_file)\n",
    "\n",
    "output_data.to_netcdf(path=download_file)\n",
    "print ('Data saved on disk!')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e5954770",
   "metadata": {},
   "source": [
    "**Note**: the dataset could be unavailable locally on your machine, but it has been indeed generated on the host machine."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c5c7c61",
   "metadata": {},
   "source": [
    "## Remove all files from the target directory\n",
    "\n",
    "To clean up the target directory at the end of the notebook, you just need to execute the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "25260aaa",
   "metadata": {},
   "outputs": [],
   "source": [
    "files_to_remove = os.listdir(target_dir)\n",
    "\n",
    "for file in files_to_remove:\n",
    "    if file.endswith(\".nc\") or file.endswith(\".txt\"):\n",
    "        os.remove(os.path.join(target_dir, file))\n",
    "print(os.listdir(target_dir))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "96c3ecd8",
   "metadata": {},
   "source": [
    "<a href=\"../00_index.ipynb\"><< Index</a><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68bdd875",
   "metadata": {},
   "source": [
    "<p><img src='../img/EUMETSAT_Logo.png' align='left' alt='Logo EUMETSAT' width='15%'></img></p>\n",
    "<br clear=left>\n",
    "<p style=\"text-align:left;\">This project is licensed under the <a href=\"../LICENSE\">MIT License</a> | \n",
    "<span style=\"float:right;\"><a href=\"https://gitlab.eumetsat.int/data-tailor/acdc-notebooks\">View on GitLab</a> | <a href=\"https://training.eumetsat.int/\">EUMETSAT Training</a></p>"
   ]
  }
 ],
 "metadata": {
  "description": "This notebook shows how to obtain a Data Cube containing Atmospheric Composition variables, such as CO, HCOOH, and NH3, using the EUMETSAT Data Tailor. For this example, Metop-A Level 3 data from IASI are used. The long-term trend of these variables is then visualised in specified cities on the earth.",
  "image": "../img/EUMETSAT_Logo.png",
  "kernelspec": {
   "display_name": "acdc-training",
   "language": "python",
   "name": "acdc-training"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.12"
  },
  "tags": {
   "category": "data_cube",
   "domain": "atmosphere",
   "level": "L3",
   "satellite": "Metop-A",
   "sensor": "IASI",
   "variable": [
    "atmosphere_mole_content_of_carbon_monoxide",
    "atmosphere_mole_content_of_formic_acid",
    "atmosphere_mole_content_of_ammonia"
   ]
  },
  "title": "Multiple IASI products combined with Data Tailor"
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
