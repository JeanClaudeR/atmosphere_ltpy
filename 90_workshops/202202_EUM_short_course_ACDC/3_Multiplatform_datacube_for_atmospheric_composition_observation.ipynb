{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "e65c162f",
   "metadata": {},
   "source": [
    "<img src='../img/EUMETSAT_Logo.png' alt='Logo EUMETSAT' align='right' width='30%'></img>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "03d76af9",
   "metadata": {},
   "source": [
    "<br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68d12e5b",
   "metadata": {},
   "source": [
    "<a href=\"../00_index.ipynb\"><< Index</a><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1bebb1c3",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "<b>60 - DATA CUBE</b></div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "67407196",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8c85eec",
   "metadata": {},
   "source": [
    "# Metop-A/B/C GOME-2 - Absorbing Aerosol Index (AAI) - Level 3 with Data Tailor"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "707a3f9a",
   "metadata": {},
   "source": [
    "This is a Python-based training module presenting how to use the **EUMETSAT Atmospheric Composition Data Cube** (ACDC) plugin for the **EUMETSAT Data Tailor** to work with satellite-derived geophysical variables relevant for atmospheric monitoring.\n",
    "\n",
    "## Introduction\n",
    "\n",
    "The **Data Tailor ACDC plugin** simplifies the process to manipulate data and thus visualise the datasets on maps.\n",
    "Specifically, the plugin features the following functionalities:\n",
    "* it downloads the desired data from the reference website\n",
    "* it allows cropping a region of interest (ROI) \n",
    "* it allows defining filters to save only the variables of interest (layer filter)\n",
    "* it allows defining the time coverage of the dataset (accordigly to the available input data)\n",
    "* it converts input products to NetCDF format compliant with CF-conventions and Climate Data Store ([CDS](https://cds.climate.copernicus.eu)) Common Data Model with coordinates dimensions\n",
    "* it allows time aggrgegation returning a single dataset\n",
    "* it estimates the output products size in advance.\n",
    "\n",
    "This notebook presents an example similar to this [Atmospheric Composition Data notebook](https://gitlab.eumetsat.int/eumetlab/atmosphere/atmosphere/-/blob/master/20_data_discovery/214_Metop-ABC_GOME-2_AAI_L3_load_browse.ipynb) which focuses on *GOME-2 Metop-A/B/C Absorbing Aerosol Index (AAI)* data\n",
    "provided by\n",
    "[EUMETSAT Satellite Application Facility on Atmospheric Composition Monitoring (AC SAF)](https://acsaf.org/index.html). You will see how to use the ACDC plugin for data retrieval, customisation and time-aggregation.\n",
    "The example shows the dispersion of aerosols in January 2020 due to the Australian fires that happened on 28 December 2019.\n",
    "To this purpose, the daily Level 3 data products of Metop-A, -B, and -C satellites for 24 consecutive days between `28 December 2019 and 20 January 2020` will be combined and visualised on maps. \n",
    "\n",
    "In this tutorial **you will learn how to use the following ACDC plugin features**:\n",
    "* retrieve the list of supported products\n",
    "* define the configuration to obtain the GOME-2 data from Metop-A/B/C satellites specifying\n",
    "    * a time period\n",
    "    * a layer filter for the *Absorbing Aerosol Index* variable\n",
    "    * time aggregation\n",
    "* invoke the Data Tailor interface\n",
    "* generate the output products in NetCDF format.\n",
    "\n",
    "With the output products, you will:\n",
    "* combine data from the three satellites \n",
    "* visualise data on a static `2D Map`\n",
    "* produce an animation of the combined data showing the aerosol dispersion in January 2020.\n",
    "\n",
    "This notebook is designed for *medium-level* users with basic knowledge about Python and Atmospheric composition data."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8c64f92",
   "metadata": {},
   "source": [
    "### Module outline:\n",
    "* [1 - Obtain AAI data using the *Atmospheric Composition Data Cube* plugin](#Obtain-AAI-data-using-the-Atmospheric-Composition-Data-Cube-plugin)\n",
    "* [2 - Concatenate data from the three satellites Metop-A, -B and -C](#Concatenate-data-from-the-three-satellites-Metop-A,--B-and--C)\n",
    "* [3 - Visualise AAI combined data](#Visualise-AAI-combined-data)\n",
    "* [4 - Save the data cube locally](#Save-the-data-cube-locally)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "49490372",
   "metadata": {},
   "source": [
    "<hr>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b4193034",
   "metadata": {},
   "source": [
    "### Load required external libraries"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c90a4dd3",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "from pathlib import Path\n",
    "import xarray as xr\n",
    "\n",
    "import IPython\n",
    "from matplotlib import animation\n",
    "from matplotlib import pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd031414-6b9f-49a2-9b97-15ed4493c585",
   "metadata": {},
   "outputs": [],
   "source": [
    "os.environ['PATH'] = f\"/opt/conda/envs/acdc-training/bin/:{os.environ['PATH']}\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eb47c2dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Load auxiliary functions\n",
    "%run ../functions.ipynb"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "040d413c",
   "metadata": {},
   "source": [
    "## Obtain AAI data using the *Atmospheric Composition Data Cube* plugin"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a06053e6",
   "metadata": {},
   "source": [
    "To use the functionalities of the ACDC plugin, you only need to import the **Data Tailor API**."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b5dd41dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "from epct import api"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d407d433",
   "metadata": {},
   "source": [
    "### Define the Data Tailor configuration"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fd8c20f1",
   "metadata": {},
   "source": [
    "This section contains the steps necessary to define valid Data Tailor configurations to obtain all the products you want to be included in your data cube.\n",
    "\n",
    "Let's start visualising the supported satellite products and how to specify them in the Data Tailor interface. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2bc5d093",
   "metadata": {},
   "source": [
    "#### Select the satelllite products\n",
    "\n",
    "In the Data Tailor API interface, a target product is referred to as a specific *product_id*. \n",
    "\n",
    "To discover which products are supported by the ACDC plugin and their corresponding *product_id*, you can use the *Data Tailor API* `read` function specifying the `epct_acdc` backend, as shown below.\n",
    "\n",
    "\n",
    "**Note**: see [Index notebook](../00_index.ipynb) to obtain also the data web-provider of each product in a \"fancy\" print. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec167527",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Obtain the dictionary of supported products\n",
    "products = api.read(\"products\", backend=\"epct_acdc\")\n",
    "\n",
    "# The product_ids are the keys of the dictionary \n",
    "for key, value in products.items():\n",
    "    print(value[\"name\"])\n",
    "    print('product-id:', key, '\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41af1f69",
   "metadata": {},
   "source": [
    "From the above list, you can spot which is the product-id corresponding to the product you want to include in your data cube.\n",
    "\n",
    "For this example, let's include three different satellite data: `GOME2_METOPA_AAI`, `GOME2_METOPB_AAI` and `GOME2_METOPC_AAI` \n",
    "\n",
    "Note that you can request one product per time using the Data Tailor interface. Therefore, define a \"product_id\" variable for each satellite data: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9b858864",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input products for Data Cube\n",
    "product_id_A = 'GOME2_METOPA_AAI'\n",
    "product_id_B = 'GOME2_METOPB_AAI'\n",
    "product_id_C = 'GOME2_METOPC_AAI'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c2607b31",
   "metadata": {},
   "source": [
    "#### Define the time coverage\n",
    "\n",
    "As said, we want to observe the three satellites for 24 consecutive days between `28 December 2019` and `20 January 2020` to see the dispersion of aerosols in January 2020 due to the Australian fires.\n",
    "\n",
    "Let us verify that the required period is available for the specified products using the *Data Tailor API* function `config`. An example of how to retrive such information for the three products is given below:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0ddeb242",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Discover the available temporal availability\n",
    "config = api.config()\n",
    "for product in [product_id_A, product_id_B, product_id_C]:\n",
    "    product_specs = config['backends_configurations']['epct_acdc']['INPUT_PRODUCTS'][product]\n",
    "    print(product)\n",
    "    print(\"periodicity: {}\".format(product_specs['periodicity']))\n",
    "    print(\"start-date: {}\".format(product_specs['start_date']))\n",
    "    if 'end_date' in product_specs:\n",
    "        print(\"stop-date: {}\".format(product_specs['end_date']))\n",
    "    print('\\n')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b688c5ae",
   "metadata": {},
   "source": [
    "This output tells us that the three satellite data have different \"start-date\" but all them continue to be acquired  as the \"stop-date\" is not available.\n",
    "\n",
    "From the above print, it is verified that the period from `28 December 2019` to `20 January 2020` is covered by all the three satellites. \n",
    "\n",
    "To select a time subset, you have to define the following two variables:\n",
    "* *sensing_start*: a date equal to or later than the available \"start-date\"\n",
    "* *sensing_stop*: a date earlier than or equal to the available \"stop-date\", if any\n",
    "\n",
    "Since you want to obtain the data from the three products in the same period, you can use the same definitions for all of them.\n",
    "\n",
    "**Note**: dates must be defined in the following format: $$'<year><month><day>T000000Z'$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4ef0285",
   "metadata": {},
   "outputs": [],
   "source": [
    "sensing_start = '20191228T000000Z'\n",
    "sensing_stop  = '20200120T000000Z'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de54611b",
   "metadata": {},
   "source": [
    "**Note**: by default, if you don't specify this arguments in the customisation call, the plugin retrieves automatically all the available time stamps from the reference web provider. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b8c33545",
   "metadata": {},
   "source": [
    "#### (Optional) Select the layer(s) of interest \n",
    "\n",
    "Each product contains a list of layers. Using the Data Tailor interface, you can filter only the layers you need.\n",
    "\n",
    "Use the Data Tailor API `epct_read` function to print the list of available variables. \n",
    "The following cell shows you an example for the Metop-A satellite data.\n",
    "\n",
    "**Note**: if you don't define a layer-filter, by default the ACDC plugin stores all the available variables in the output dataset."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "208cc2bd",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define a printing function for the selected product available bands \n",
    "def print_product_bands(product_id):\n",
    "    \n",
    "    # Obtain the list of available bands\n",
    "    products = api.read(f\"products/{product_id}\")\n",
    "    bands=products['bands']\n",
    "    # print the list\n",
    "    print(\"{} bands: \\n\".format(product_id))\n",
    "    for key, value in bands.items():  \n",
    "         print(\"{}) {}: {}\".format(value['number'], key, value['name']))\n",
    "            \n",
    "             \n",
    "\n",
    "# Display the list calling the above printing function\n",
    "print_product_bands(product_id_A)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4822b755",
   "metadata": {},
   "source": [
    "As you can see, the product contains 3 different variables. \n",
    "\n",
    "For each supported product, there are also pre-defined filters.\n",
    "Let's print the available pre-defined filters for the selected products using the Data Tailor API `read` function with the `\"filters\"` argument:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2d28f13f",
   "metadata": {},
   "outputs": [],
   "source": [
    "for product in [product_id_A, product_id_B, product_id_C]:\n",
    "    product_filter = api.read(\"filters\", product=product)\n",
    "    print(product_filter)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28c30577",
   "metadata": {},
   "source": [
    "To select the main variable of these products, i.e. the `absorbing_aerosol_index`, you can use the corresponding pre-defined filters: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "689a6535",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input product band selection\n",
    "layer_filter_A = 'gome2_metopa_aai_main'\n",
    "layer_filter_B = 'gome2_metopb_aai_main'\n",
    "layer_filter_C = 'gome2_metopc_aai_main'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c0c4e602",
   "metadata": {},
   "source": [
    "This is equivalent to define the filter in a dictionary containing the key `bands`, as follows. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3b4551ae",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Input product band selection\n",
    "layer_filter = {'bands': ['absorbing_aerosol_index']}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "76395432",
   "metadata": {},
   "source": [
    "Note that, for this example, it would be convenient to use the dictionary definition which is equivalent for all the three products. However, you will use the pre-defined filters to see how to specify different filters in the Data Tailor configuration."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c36db5ed",
   "metadata": {},
   "source": [
    "### Generate your Atmospheric Composition Data Cube\n",
    "\n",
    "To retrieve and customise the three different products, you have to invoke the *Data Tailor API* `run_chain` function three times (one for each satellite data). \n",
    "\n",
    "This action will: \n",
    "* download data from the reference web provider\n",
    "* convert input data in CF-compliant homogenised NetCDF format\n",
    "* perform time aggregation\n",
    "* return the paths to the output files\n",
    "\n",
    "To invoke `run_chain` you need to specify:\n",
    "* an input file\n",
    "* a directory to store the outputs\n",
    "* the customisation configuration\n",
    "* the start-date and end-date of the period of interest\n",
    "\n",
    "Since the data are automatically retrieved from web-provider, you don't need a real input file. So, let's create a dummy file instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c07ab914",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create a dummy file\n",
    "Path('dummy.txt').touch()\n",
    "product_paths = ['dummy.txt']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9e649f76",
   "metadata": {},
   "source": [
    "Let's define the output path:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1d9cebe2",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Define the output path\n",
    "target_dir='.'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "20a28b89",
   "metadata": {},
   "source": [
    "Now, you need to define the configuration as a dictionary. \n",
    "For this example, the dictionary must have the following keys:\n",
    "* *product* (mandatory) \n",
    "* *format* (mandatory): use \"*netcdf4_acdc*\" to specify the conversion to NetCDF obedient to CF-convention\n",
    "* *aggregation* (optional): if you want the output to be a single dataset containing all time-stamps\n",
    "* *filter* (optional): to filter only the `AAI` variable\n",
    "\n",
    "**Note**: To retrieve the three different products, define the configuration dictionary and invoke the customisation function in a `for loop`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9e762447",
   "metadata": {},
   "outputs": [],
   "source": [
    "output_products = []\n",
    "for product, layer in [(product_id_A, layer_filter_A), (product_id_B, layer_filter_B), (product_id_C, layer_filter_C)]:\n",
    "    # Define the chain_config dictionary with the current product\n",
    "    chain_config = {\n",
    "        \"product\": product,\n",
    "        \"format\": \"netcdf4_acdc\",\n",
    "        \"aggregation\": \"time\",\n",
    "        \"filter\": layer\n",
    "    }\n",
    "    #Invoke run_chain function\n",
    "    output_paths = api.run_chain(\n",
    "        product_paths, \n",
    "        chain_config=chain_config, \n",
    "        target_dir=target_dir, \n",
    "        sensing_start=sensing_start,\n",
    "        sensing_stop=sensing_stop\n",
    "    )\n",
    "    output_products.extend(output_paths)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9214a78a",
   "metadata": {},
   "source": [
    "The variable `output_products` is a list containing the output dataset paths of the Data Tailor customisations. In the following, you can use the `output_products` variable to load the obtained dataset.\n",
    "\n",
    "Let's visualise the content of the **output_products** variable:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7102ae0b",
   "metadata": {},
   "outputs": [],
   "source": [
    "output_products"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5d74148c",
   "metadata": {},
   "source": [
    "As you can see, the output consists of three NetCDF files (one for each satellite). In the following, you can use the `output_products` variable to load the obtained dataset."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "73396d70",
   "metadata": {},
   "source": [
    "### Visualise the structure of an output file\n",
    "\n",
    "Let's explore the Data Tailor output products data structure by opening one file with `xarray`. As you can see, the dataset is CF-compliant and contains all the required metadata, such as: \n",
    "* CF-convention version\n",
    "* DOI or reference to the product webpage\n",
    "* data provider and contacts\n",
    "* product version\n",
    "* start (and end, if any) dataset date(s)\n",
    "* satellites and sensors involved in the creation of the data record\n",
    "* “history” attribute tracking commands and version of the used software for operation to generate the output.\n",
    "\n",
    "As an example you can observe the dataset corresponding to the GOME2 Metop-A satellite.\n",
    "\n",
    "**Note**: to guarantee that the dataset is CDS and CF-compliant,\n",
    "in case of new variable names, a proposal for the variable name has been added in the\n",
    "`tentative_standard_name` attribute "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45595357",
   "metadata": {},
   "outputs": [],
   "source": [
    "example_data = xr.open_dataset(output_products[0])\n",
    "example_data"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "270e0096",
   "metadata": {},
   "source": [
    "As you can see the dataset contains: \n",
    "* only the variable `aai` as you used the layer-filter functionality\n",
    "* all the requested time-stamps as you asked to the Data Tailor interface to perform time-aggregation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "60b5532f",
   "metadata": {},
   "source": [
    "## Concatenate data from the three satellites Metop-A, -B and -C\n",
    "\n",
    "The overall goal is to bring the AAI data from all three satellites together. Thus, the next step is to concatenate the three `time aggregation datasets` along a new dimension that we call `satellite`. \n",
    "You can use the `concat()` function from the `xarray` library.\n",
    "\n",
    "The result is a four-dimensional `xarray.Dataset`, with the dimensions `satellite`, `time`, `latitude` and `longitude` already having proper coordinates."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a8257a3",
   "metadata": {},
   "source": [
    "To this purpose, you need to load the three satellite datasets using xarray: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17fe9ae1",
   "metadata": {},
   "outputs": [],
   "source": [
    "AAI_Metop_A = xr.open_dataset(output_products[0])\n",
    "AAI_Metop_B = xr.open_dataset(output_products[1])\n",
    "AAI_Metop_C = xr.open_dataset(output_products[2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "57afc217",
   "metadata": {},
   "outputs": [],
   "source": [
    "aai_concat = xr.concat([AAI_Metop_A, AAI_Metop_B, AAI_Metop_C], dim='satellite')\n",
    "aai_concat"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4466a0b4",
   "metadata": {},
   "source": [
    "Since the final aim is to combine the data from the three satellites Metop-A, -B and -C onto one single grid, the next step is to reduce the `satellite` dimension. You can do this by applying the reduce function `mean` to the `aai_concat` dataset. The dimension (`dim`) to be reduced shall be the `satellite` dimension."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4cf280be",
   "metadata": {},
   "outputs": [],
   "source": [
    "aai_combined = aai_concat.mean(dim='satellite')\n",
    "aai_combined"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4abcba92",
   "metadata": {},
   "source": [
    "**Note**: in this operation, you will lose all the datasets' attributes. However, having common characteristics, you can still use the single satellite dataset attributes for plotting features."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1cb46216",
   "metadata": {},
   "source": [
    "## Visualise AAI combined data\n",
    "\n",
    "\n",
    "Now that you have your desired data stored and homogenised in a single dataset, you can easily visualise it on map. In the following, we will show you how to produce: \n",
    "* a static `2D Map` of the daily Metop-A/B/C GOME-2 Level 3 Absorbing Aerosol Index (AAI) on a selected date\n",
    "* an animation of the AII dispersion between 28 December 2019 and 20 January 2020\n",
    "\n",
    "For this purpose, you have to select the AII data variable. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "64367ea1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select aai variable\n",
    "aai = aai_combined.aai "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "53a08f6f",
   "metadata": {},
   "source": [
    "### Static `2D Map` of the daily Metop-A/B/C GOME-2 Level 3 Absorbing Aerosol Index (AAI) on a selected date\n",
    "\n",
    "You can make use of the function [visualize_pcolormesh](utils.ipynb#visualize_pcolormesh) which takes the following arguments:\n",
    "* `data_array`\n",
    "* `longitude`\n",
    "* `latitude`\n",
    "* `projection`\n",
    "* `color_scale`\n",
    "* `unit`\n",
    "* `long_name`\n",
    "* `vmin`\n",
    "* `vmax`\n",
    "\n",
    "Use `afmhot_r` as color map, `ccrs.PlateCarree()` as projection and by applying `dt.strftime('%Y-%m-%d').data` to the time coordinate variable, you can add the valid time step to the title of the plot.\n",
    "\n",
    "The resulting plot shows the AAI levels increment on the east coast of Australia on 28 December 2019 due to the wildfire.\n",
    "\n",
    "**Note**: you can easily visualise the data on other dates, changing the value of the `day` variable defined below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2718f811",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Select the index 0 which corresponds to 2019/12/27 (try also other dates)\n",
    "day = 0 \n",
    "# Retrieve the long_name from the initial dataset \n",
    "long_name = aai_concat.aai.long_name\n",
    "aai = aai.transpose(..., \"lat\", \"lon\")\n",
    "\n",
    "visualize_pcolormesh(data_array=aai.isel(time=day),\n",
    "                     longitude=aai.lon, \n",
    "                     latitude=aai.lat,\n",
    "                     projection=ccrs.PlateCarree(), \n",
    "                     color_scale='afmhot_r', \n",
    "                     unit= '',\n",
    "                     long_name=long_name + ': ' + str(aai.time[day].dt.strftime('%Y-%m-%d').data), \n",
    "                     vmin=0, \n",
    "                     vmax=5, \n",
    "                     lonmin=-180, \n",
    "                     lonmax=179.9, \n",
    "                     latmin=-90., \n",
    "                     latmax=90.,\n",
    "                     set_global=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "11755de3",
   "metadata": {},
   "source": [
    "### Animation of the AII dispersion between 28 December 2019 and 20 January 2020\n",
    "\n",
    "Now, let's see how to animate the `aai_combined` dataset over the 24 days to see the dispersion of Aerosols resulting from biomass burning during the severe Australian Fires in January 2020.\n",
    "\n",
    "The animation function consists of four parts:\n",
    "- **Setting the initial state:**<br>\n",
    " Here, you define the general plot your animation shall use to initialise the animation. You can also define the number of frames (time steps) your animation shall have.\n",
    " \n",
    " \n",
    "- **Functions to animate:**<br>\n",
    " An animation consists of three functions: `draw()`, `init()` and `animate()`. `draw()` is the function where individual frames are passed on and the figure is returned as image. In this example, the function redraws the plot for each time step. `init()` returns the figure you defined for the initial state. `animate()` returns the `draw()` function and animates the function over the given number of frames (time steps).\n",
    " \n",
    " \n",
    "- **Create a `animate.FuncAnimation` object:** <br>\n",
    " The functions defined before are now combined to build an `animate.FuncAnimation` object.\n",
    " \n",
    " \n",
    "- **Play the animation as video:**<br>\n",
    " As a final step, you can integrate the animation into the notebook with the `HTML` class. You take the generate animation object and convert it to a HTML5 video with the `to_html5_video` function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7ab3aaa1",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Setting the initial state (figure for initial plot)\n",
    "day=0\n",
    "fig, ax = visualize_pcolormesh(data_array=aai.isel(time=day),\n",
    "                               longitude=aai.lon, \n",
    "                               latitude=aai.lat,\n",
    "                               projection=ccrs.PlateCarree(), \n",
    "                               color_scale='afmhot_r', \n",
    "                               unit=' ',\n",
    "                               long_name=long_name + '/' + str(aai_combined.time[0].dt.strftime('%Y-%m-%d').data), \n",
    "                               vmin=0, \n",
    "                               vmax=10, \n",
    "                               lonmin=-180, \n",
    "                               lonmax=180, \n",
    "                               latmin=-90., \n",
    "                               latmax=90.,\n",
    "                               set_global=True)\n",
    "\n",
    "# Define the number of frames using the Dataset time dimension length \n",
    "frames = len(aai_combined.time)\n",
    "\n",
    "\n",
    "def draw(i):\n",
    "    img = plt.pcolormesh(aai.lon, \n",
    "                         aai.lat, \n",
    "                         aai[i,:,:], \n",
    "                         cmap='afmhot_r', \n",
    "                         transform=ccrs.PlateCarree(),\n",
    "                         vmin=0,\n",
    "                         vmax=10)\n",
    "    ax.set_title(long_name + ' ' + str(aai_combined.time[i].dt.strftime('%Y-%m-%d').data),\n",
    "                fontsize=20, pad=20.0)\n",
    "    return img\n",
    "\n",
    "\n",
    "def init():\n",
    "    return fig\n",
    "\n",
    "\n",
    "def animate(i):\n",
    "    return draw(i)\n",
    "\n",
    "ani = animation.FuncAnimation(fig, animate, frames, interval=800, blit=False,\n",
    "                              init_func=init, repeat=True)\n",
    "\n",
    "HTML(ani.to_html5_video())\n",
    "plt.close(fig)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "030bc956",
   "metadata": {},
   "source": [
    "#### Play the animation as HTML5 video"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f8651343",
   "metadata": {},
   "source": [
    "The cell below makes the output cell not scrollable"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7f08aba9",
   "metadata": {},
   "outputs": [],
   "source": [
    "%%javascript\n",
    "IPython.OutputArea.prototype._should_scroll = function(lines) {\n",
    "    return false;\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "746ae737",
   "metadata": {},
   "outputs": [],
   "source": [
    "IPython.display.HTML(ani.to_html5_video())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef6a67a1",
   "metadata": {},
   "source": [
    "## Save the data cube locally\n",
    "\n",
    "You can download the data cube locally by executing the following cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c4f3a6f1",
   "metadata": {},
   "outputs": [],
   "source": [
    "download_file = './AII_data.nc'\n",
    "print ('Saving to: ', download_file)\n",
    "\n",
    "aai_combined.to_netcdf(path=download_file)\n",
    "print ('Data saved on disk!')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9db99792",
   "metadata": {},
   "source": [
    "**Note**: the dataset could be unavailable locally on your machine, but it has been indeed generated on the host machine."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "64b66f03",
   "metadata": {},
   "source": [
    "## Remove all files from the target directory\n",
    "\n",
    "To clean up the target directory at the end of the notebook, you just need to execute the cell below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e2ada33a",
   "metadata": {},
   "outputs": [],
   "source": [
    "files_to_remove = os.listdir(target_dir)\n",
    "\n",
    "for file in files_to_remove:\n",
    "    if file.endswith(\".nc\") or file.endswith(\".txt\"):\n",
    "        os.remove(os.path.join(target_dir, file))\n",
    "print(os.listdir(target_dir))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ceb74b77",
   "metadata": {},
   "source": [
    "<a href=\"../00_index.ipynb\"><< Index</a><br>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "108ab806",
   "metadata": {},
   "source": [
    "<p><img src='../img/EUMETSAT_Logo.png' align='left' alt='Logo EUMETSAT' width='15%'></img></p>\n",
    "<br clear=left>\n",
    "<p style=\"text-align:left;\">This project is licensed under the <a href=\"../LICENSE\">MIT License</a> | \n",
    "<span style=\"float:right;\"><a href=\"https://gitlab.eumetsat.int/data-tailor/acdc-notebooks\">View on GitLab</a> | <a href=\"https://training.eumetsat.int/\">EUMETSAT Training</a></p>"
   ]
  }
 ],
 "metadata": {
  "description": "This notebook shows how to obtain GOME-2 Metop-A/B/C Absorbing Aerosol Index (AAI) data with the EUMETSAT Data Tailor and combine them in a single dataset. The obtained dataset is used to visualise the aerosols dispersion in January due to the Australian fires that happened on 28 December 2019 with an animation.",
  "image": "../img/EUMETSAT_Logo.png",
  "kernelspec": {
   "display_name": "acdc-training",
   "language": "python",
   "name": "acdc-training"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.12"
  },
  "tags": {
   "category": "data_cube",
   "domain": "atmosphere",
   "level": "L3",
   "satellite": "Metop-A/B/C",
   "sensor": "GOME-2",
   "variable": "absorbing_aerosol_index"
  },
  "title": "Metop-A/B/C GOME-2 Absorbing Aerosol Index (AAI) Level 3 with Data Tailor"
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
