{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# HARP comparison\n",
    "\n",
    "This practical will show you how to compare Sentinel-5P satellite data against ground based data.\n",
    "In this exercise we will be focusing primarily on HARP as the toolset to do this."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will use Sentinel-5P Level2 NO2 data and compare this against a MAXDOAS instrument that is located in Athens, Greece.\n",
    "MAXDOAS is a DOAS instrument, just like the Pandora instruments. However, MAXDOAS is a MAXDOAS type instrument and Pandora uses the DirectSun approach.\n",
    "\n",
    "You can find an explanation on the different measurement techniques on the [FRM4DOAS website](https://frm4doas.aeronomie.be/index.php/project-overview/doas)\n",
    "![doas-techniques](https://frm4doas.aeronomie.be/ProjectDir/doasinstruments.png)\n",
    "\n",
    "The main difference to be aware of is the altitude range for which the measurements are applicable.\n",
    "\n",
    "The MAXDOAS measurements only provide information on the troposphere, so we will use this data to compare against the tropospheric NO2 column information from S5P (`tropospheric_NO2_column_number_density` variable).\n",
    "\n",
    "The Pandora measurements, on the other hand, provide information on the total column. So we could use that data to compare against the total NO2 column from S5P (`NO2_column_number_density` variable)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For this exercise we will look at data from February 2020.\n",
    "\n",
    "The Sentinel-5P data was retrieved from the [Copernicus Data Space Ecosystem](https://dataspace.copernicus.eu).\n",
    "\n",
    "The MAXDOAS data was retrieved from [NDACC](https://www-air.larc.nasa.gov/missions/ndacc/data.html#). This data (but also Pandora data) is also available through [EVDC](https://evdc.esa.int)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## S5P vs. MAXDOAS NO2 comparison\n",
    "\n",
    "In order to perform a comparison for the full month of February 2020, we would need a full month of Sentinel-5P data.\n",
    "Even if we already filter for only those products that have data over Athens, we would still end up with about 48 orbits (note that, because orbits slightly overlap, we have multiple overpasses within a single day for some days).\n",
    "\n",
    "Since we are only interested in the data around Athens, we ideally don't want keep the full 450MB for each L2 product, but only the satellite data around the area.\n",
    "\n",
    "A convenient first step is therefore to create so-called _overpass files_. We can do this with HARP, by providing a geographic filter on the location of the MAXDOAS instrument, which is at 38.05 latitude and 23.86 longitude.\n",
    "\n",
    "As an example we will perform such a filter on the NO2 data from the regridding exercise (which was data from 15 September 2020)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import avl\n",
    "import harp\n",
    "import csv\n",
    "import numpy as np\n",
    "datadir = \"/home/jovyan/eodata/ac_training_school\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = datadir + \"/sentinel-5p/level2/S5P_RPRO_L2__NO2____20200915T002200_20200915T020329_15147_03_020400_20221105T064431.nc\"\n",
    "# since the area_covers_point filter is quite slow, we add some explicit filters\n",
    "# on latitude (which is fast) to already exclude a large part of the product\n",
    "operations = \"latitude>36;latitude<40;area_covers_point(38.05, 23.86)\"\n",
    "try:\n",
    "    overpass = harp.import_product(filename, operations)\n",
    "except harp.NoDataError:\n",
    "    print('No overpasses found')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We see that this product did not contain any matches. If that happens you get an error which you can catch using this `try`/`catch` approach.\n",
    "\n",
    "If we try this filter for a product that actually does contain an overpass we get:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = datadir + \"/sentinel-5p/level2/S5P_RPRO_L2__NO2____20200915T103056_20200915T121226_15153_03_020400_20221105T064441.nc\"\n",
    "operations = \"latitude>36;latitude<40;area_covers_point(38.05, 23.86)\"\n",
    "try:\n",
    "    overpass = harp.import_product(filename, operations)\n",
    "except harp.NoDataError:\n",
    "    print('No overpasses found')\n",
    "print(overpass)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see that we only got one measurement for each variable.\n",
    "\n",
    "Instead of reading this data in Python, we actually want to have this data stored as a file on disk.\n",
    "This allows us to reuse it later as input for our comparisons (and we can then throw away the original L2 products).\n",
    "\n",
    "To do this we could use a combination of `harp.import_product()` and `harp.export_product()` in Python.\n",
    "However, it is actually faster to call the `harpconvert` tool from the command line.\n",
    "\n",
    "You can call command line tools from within a python notebook by prefixing the command with a `!`.\n",
    "This is an IPython feature that is described in the [documentation](https://ipython.readthedocs.io/en/stable/interactive/python-ipython-diff.html#shell-assignment).\n",
    "We will use this several times in this exercise.\n",
    "\n",
    "Be aware that the commands that we will execute are Linux-style commands which will work on Linux and macOS, but may not work on Windows (without some modification to path references and/or usage of quotes).\n",
    "\n",
    "To convert the product using `harpconvert` we can use:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!harpconvert -a \"latitude>36;latitude<40;area_covers_point(38.05, 23.86)\" {datadir}/sentinel-5p/level2/S5P_RPRO_L2__NO2____20200915T103056_20200915T121226_15153_03_020400_20221105T064441.nc s5p_l2_no2_15153_athens.nc"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "And we can then read in this overpass file in Python using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "overpass = harp.import_product(\"s5p_l2_no2_15153_athens.nc\")\n",
    "print(overpass)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the product contains a `history` attribute that shows how HARP generated the file.\n",
    "HARP will include such history information in each file that it writes, which is very useful for traceability."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(overpass.history)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For the month of February we already created such overpass files, which are available in the `eodata/....` directory. These files are actually the official overpass files that are used by the [Sentinel-5P Mission Performance Center Validation Facility](http://mpc-vdaf.tropomi.eu).\n",
    "\n",
    "These files contain not just the pixel over Athens itself, but also a range of pixels around that area. This allows the validation experts to investigate other criteria such as the spatial homogeneity of the data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = datadir + \"/sentinel-5p/overpasses/athens/S5P_RPRO_L2VONO2____20200201T094106_20200201T112236_11932_03_020400_20221107T230009_athens.nc\"\n",
    "overpass_11932 = harp.import_product(filename)\n",
    "print(overpass_11932)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see from the `history` attribute this overpass file was just a filtering of the original proudct using a polygon area; no other HARP operations were performed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use AVL to plot this overpass data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avl.Geo(overpass_11932, 'tropospheric_NO2_column_number_density', colorrange=(0,0.0001),\n",
    "        centerlat=38.05, centerlon=23.86, zoom=7)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we have the satellite data, we can start collocating the data with the MAXDOAS data.\n",
    "\n",
    "What we want, is to know which satellite measurements match up in time and space with which MAXDOAS measurements.\n",
    "\n",
    "The `harpcollocate` command line tool is designed to answer this question. It will take distance criteria on e.g. time and space and produce a list of pairs of measurements where the satellite and reference data match.\n",
    "\n",
    "You can get a quick help reference by passing the `--help` argument to the harpcollocate tool."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!harpcollocate --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As a time distance criterium we are interested in measurements that are only half an hour apart. And for the spatial distance, we are only interested on satellite pixels that are directly over the MAXDOAS instrument.\n",
    "\n",
    "The command with this criteria will then be:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!harpcollocate -d \"datetime 0.5 [h]\" --point-in-area-yx {datadir}/sentinel-5p/overpasses/athens {datadir}/maxdoas/athens collocations_maxdoas_full.csv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This command produced a `csv` file called `collocations_maxdoas_full.csv` that contains the matching pairs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open('collocations_maxdoas_full.csv') as csv_file:\n",
    "    csv_reader = csv.reader(csv_file, delimiter=',')\n",
    "    for row in csv_reader:\n",
    "       print(', '.join(row))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What you will see on each line is:\n",
    "- a unique identifier of the collocation pair (the `collocation_index`)\n",
    "- a reference to the satellite product\n",
    "- an index of the measurement within the satellite product\n",
    "- a reference to the maxdoas product\n",
    "- an index of the measurement within the maxdoas product\n",
    "- the distance (in time) between the two measurements\n",
    "\n",
    "Note that the reference to the satellite product is the orginal L2 product. Also, the 'index' of the satellite measurement is the index of the pixel in the original L2 product (this index value is stored as an `index` variable in the overpass file).\n",
    "The advantage of this, is that you can get the measurement directly from the original L2 product again without having to have access to the overpass file.\n",
    "All the operations we perform below on the overpass files using this collocation result file can actually also still be performed on the original L2 products as well. This makes it easy to share a collocation result file with someone else who doesn't have your overpass files. That person can then download the original products and use the collocation result file to quickly extract the collocated measurements."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can see in the list that sometimes the same satellite measurement appears twice (e.g. `S5P_RPRO_L2__NO2____20200201T094106_20200201T112236_11932_03_020400_20221107T230009.nc` measurement `1450403`). This is because within the the given half hour time distance there are multiple MAXDOAS measurements that match that criteria.\n",
    "\n",
    "We can instruct HARP to only take the nearest MAXDOAS measurement in that case by providing the `-nx datetime` option to `harpcollocate`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Also, the collocations that we produced were actually not filtered for quality. We actually only want measurements that are 'of good quality'.\n",
    "For the S5P data this means applying the `tropospheric_NO2_column_number_density_validity > 75` filter and for MAXDOAS we are only going to filter out NaN values (which can be done using the `valid(tropospheric_NO2_column_number_density)` filter.\n",
    "\n",
    "We can pass these filters as part of the `harpcollocate` command line using the `-aa` and `-ab` parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If we add the `-nx` and `-aa` and `-ab` options we get:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!harpcollocate -d \"datetime 0.5 [h]\" --point-in-area-yx -nx datetime -aa \"tropospheric_NO2_column_number_density_validity > 75\" -ab \"valid(tropospheric_NO2_column_number_density)\" {datadir}/sentinel-5p/overpasses/athens {datadir}/maxdoas/athens collocations_maxdoas.csv"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now that we know which measurements pair up, we need to filter both the satellite data and the MAXDOAS data to provide us the data for those pairs.\n",
    "\n",
    "We do this by using the `collocate_left()` and `collocate_right()` HARP operations. The `collocate_left()` filters based on the information that is on the _left_ for each pair (i.e. the satellite data) and `collocate_right()` the information that is on the _right_ (i.e. the MAXDOAS data).\n",
    "\n",
    "In addition, we need to add several other operations that allow us to make sure that variables have the same units for both the satellite and maxdoas data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filepattern = datadir + \"/sentinel-5p/overpasses/athens/*\"\n",
    "operations = ';'.join([\n",
    "    'collocate_left(\"collocations_maxdoas.csv\")',\n",
    "    'derive(datetime {time} [days since 2000-01-01])',\n",
    "    'derive(tropospheric_NO2_column_number_density [Pmolec/cm2])',\n",
    "    'derive(tropospheric_NO2_column_number_density_uncertainty {time} [Pmolec/cm2])',\n",
    "    'sort(collocation_index)',\n",
    "])\n",
    "s5p = harp.import_product(filepattern, operations)\n",
    "\n",
    "filepattern = datadir + \"/maxdoas/athens/*\"\n",
    "operations = ';'.join([\n",
    "    'collocate_right(\"collocations_maxdoas.csv\")',\n",
    "    'derive(datetime {time} [days since 2000-01-01])',\n",
    "    'derive(tropospheric_NO2_column_number_density [Pmolec/cm2])',\n",
    "    'derive(tropospheric_NO2_column_number_density_uncertainty {time} [Pmolec/cm2])',\n",
    "    'sort(collocation_index)',\n",
    "])\n",
    "maxdoas = harp.import_product(filepattern, operations)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You will see that the imported s5p and maxdoas data now contain the same amount of measurements. And by sorting both datasets by the `collocation_index` we make sure that all the measurements are nicely aligned."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(s5p)\n",
    "print(maxdoas)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can now plot the s5p and maxdoas data side-by-side"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plot = avl.Scatter(s5p, \"tropospheric_NO2_column_number_density\")\n",
    "plot.add(avl.Scatter(maxdoas, \"tropospheric_NO2_column_number_density\"))\n",
    "plot._fig.layout.title.text = \"S5P vs. MAXDOAS - February 2020\"\n",
    "plot._fig.update_traces({'name': 's5p'}, 0)\n",
    "plot._fig.update_traces({'name': 'maxdoas'}, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can also plot the difference. This can be done using:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "diff = s5p.tropospheric_NO2_column_number_density.data - maxdoas.tropospheric_NO2_column_number_density.data\n",
    "err = np.sqrt(s5p.tropospheric_NO2_column_number_density_uncertainty.data**2 + maxdoas.tropospheric_NO2_column_number_density_uncertainty.data**2)\n",
    "ylabel = f'tropospheric NO2 difference (S5P-MAXDOAS) [{s5p.tropospheric_NO2_column_number_density.unit}]'\n",
    "title = \"S5P vs. MAXDOAS - February 20202\"\n",
    "avl.vis.Scatter(avl.get_timestamps(s5p), diff, yerror=err, title=title, ylabel=ylabel)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This comparison is actually also available on the [Validation Server](https://mpc-vdaf-server.tropomi.eu) that is used by the S5P Mission Performance center.\n",
    "\n",
    "You can compare the results by looking at the [report for NO2 MAXDOAS for Athens](https://mpc-vdaf-server.tropomi.eu/no2/no2-offl-maxdoas/athens?from_date=2020-02-01&to_date=2020-03-01)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
