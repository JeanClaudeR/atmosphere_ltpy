{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Basic data reading and visualisation with HARP and AVL\n",
    "\n",
    "In the steps below we will provide a basic data reading and visualization introduction."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Basic data reading with HARP"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We first import the needed Python packages. We only need harp and avl."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import avl\n",
    "import harp\n",
    "datadir = \"/home/jovyan/eodata/ac_training_school\""
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will start by importing a single Sentinel-5P L2 NO2 product.\n",
    "This product was retrieved from the [Copernicus Data Space Ecosystem](https://dataspace.copernicus.eu).\n",
    "\n",
    "Importing a product with HARP will read it completely in memory, which can be a bit much depending on the system you are running this notebook on, so we will begin with reading just one sample of everything to see what is in the product."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "filename = datadir + \"/sentinel-5p/level2/S5P_RPRO_L2__NO2____20200915T103056_20200915T121226_15153_03_020400_20221105T064441.nc\"\n",
    "product = harp.import_product(filename, operations=\"index==0\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The resulting `product` python variable is just a record containing the product variables with numpy arrays for the data. You can inspect the contents using the Python `print()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(product)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(product.tropospheric_NO2_column_number_density)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(type(product.tropospheric_NO2_column_number_density.data))\n",
    "print(product.tropospheric_NO2_column_number_density.data.shape)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is important to realise that the `harp.import_product` function does not return the data in the same structure as it can be found in the netcdf file (and how you might get it with libraries such as `netCDF4` or `xarray`). HARP really performs an _import_ and converts the data from the original Sentinel-5P L2 format to a structure that is compatible with the [HARP Conventions](http://stcorp.github.io/harp/doc/html/conventions/index.html).\n",
    "\n",
    "This conversion mainly consists of renaming of variables, restructuring the dimensions, and leaving out the 'less important' variables. HARP has builtin converters for [a lot of atmospheric data products](http://stcorp.github.io/harp/doc/html/ingestions/index.html). For each conversion the HARP documentation contains a description of the variables it will return and how it mapped them from the original product format. The description for the product that we ingested can be found in the [S5P_L2_NO2](http://stcorp.github.io/harp/doc/html/ingestions/S5P_L2_NO2.html) description.\n",
    "\n",
    "HARP does this conversion such that data from other satellite data products, such as OMI, or GOME-2, will end up having the same structure and naming conventions. This makes it a lot easier for users such as yourself to deal with data coming from different satellite missions.\n",
    "\n",
    "HARP allows for powerful operations to be performed during ingestion. This includes filtering, unit conversion, parameter conversion, regridding, etc.\n",
    "\n",
    "One import filter is to only ingest data where the validity is good enough (this is the `qa_value>0.5` filter that is mentioned in the [Product Readme File for NO2](https://sentinels.copernicus.eu/documents/247904/3541451/Sentinel-5P-Nitrogen-Dioxide-Level-2-Product-Readme-File); the PRFs for all S5P products can be found on the offical [S5P documentation page](https://sentinels.copernicus.eu/web/sentinel/technical-guides/sentinel-5p/products-algorithms))\n",
    "\n",
    "Information on all the operations can be found in the [HARP documentation](http://stcorp.github.io/harp/doc/html/operations.html).\n",
    "\n",
    "We will now perform the import will these operations combined:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "operations=\";\".join([\n",
    "    # only include properly QA valid measurements (using the filter tropospheric NO2)\n",
    "    'tropospheric_NO2_column_number_density_validity>75',\n",
    "    # perform a spatial filter to a subregion of Europe\n",
    "    'latitude>=35;latitude<=65',\n",
    "    'longitude>=-12;longitude<=35',\n",
    "    # only read specific variables\n",
    "    'keep(datetime*, latitude*, longitude*, *NO2_column_number_density)',\n",
    "    # perform unit conversion of the number densities\n",
    "    'derive(tropospheric_NO2_column_number_density [Pmolec/cm2])',\n",
    "    'derive(NO2_column_number_density [Pmolec/cm2])',\n",
    "    'derive(stratospheric_NO2_column_number_density [Pmolec/cm2])',\n",
    "])\n",
    "print(operations)\n",
    "product = harp.import_product(filename, operations=operations)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(product)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can use AVL to quickly and accurately visualize this data:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avl.Geo(product, \"tropospheric_NO2_column_number_density\", colormap=\"jet\",\n",
    "        centerlat=52, centerlon=12, zoom=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avl.Geo3D(product, \"tropospheric_NO2_column_number_density\", colormap=\"jet\",\n",
    "          centerlat=52, centerlon=12, zoom=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avl.Scatter(product, \"tropospheric_NO2_column_number_density\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avl.Histogram(product, \"tropospheric_NO2_column_number_density\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "avl.vis.Scatter(product.tropospheric_NO2_column_number_density.data,\n",
    "                product.stratospheric_NO2_column_number_density.data,\n",
    "                xlabel=\"tropospheric NO2\", ylabel=\"stratospheric NO2\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
