# Washburn Fire Case Study

This notebook provides an introduction to the Sentinel-3 OLCI and SLSTR data focusing on the [Washburn Fire](https://inciweb.nwcg.gov/incident/8209/) near Yosemite, California, USA that occurred in July 2022.

## License

This code is licensed under a GPL-3.0-only license. See file [LICENSE.txt](./LICENSE.txt) for details on the usage and distribution terms.
  
## Author

* [Sabrina Szeto](mailto://consult@sabrinaszeto.com) - [MEEO](http://www.meeo.it)


## Learning Outcomes
The course is designed for `intermediate users`, who have basic Python knowledge, basic earth observation knowledge and basic understanding of the fire life cycle.

As many Python library packages use American English, the functions also use American English for consistency. Hence, you will note that spellings for "color" and "visualize" are used instead of their British English equivalents. 


## Getting Started
   
These instructions will get you a copy of the project up and running on your local machine.
  
### Prerequisites

This course material is made available via EUMETSAT's EUMETlab training space on GitLab. While GitLab offers integrated rendering of Jupyter Notebooks, its rendering capabilities are limited. Thus, the notebooks, if directly rendered on GitLab, might appear in a less user-friendly format, which could make the learning process difficult.

#### Included components

The following components are included in this software package:

* coord2area.py, GPL-3.0 License, Copyright 2012-2019 Satpy developers, https://github.com/pytroll/satpy/blob/c20c3bb7042c819baffbb187f1ccaa9efbd74347/utils/coord2area_def.py, Location: ./coord2area_def.py


#### Dependencies
The following dependencies are not included in the package but are required and they will be downloaded at build or compilation time:

* **affine**, 2.3.0, BSD-3-Clause License, Copyright 2014 Sean C. Gillies, https://github.com/rasterio/affine
* **Cartopy**, 0.20.2, LGPL-3.0 License, Copyright British Crown. Copyright 2011 - 2016. Met Office, https://github.com/SciTools/cartopy
* **ipyleaflet**, 0.14.0, MIT License, Copyright 2014-2018 Project Jupyter Contributors, https://github.com/jupyter-widgets/ipyleaflet
* **ipython**, 7.29.0, BSD-3-Clause License, Copyright 2008-Present IPython Development Team, https://github.com/ipython/ipython
* **ipywidgets**, 7.6.5, BSD-3-Clause License, Copyright 2015 Project Jupyter Contributors, https://github.com/jupyter-widgets/ipywidgets
* **jupyter-client**, 7.0.6, BSD 3-Clause License, Copyright 2015-Present Jupyter Development Team, https://github.com/jupyter/jupyter_client
* **jupyter-core**, 4.9.1, BSD 3-Clause License, Copyright 2015-Present Jupyter Development Team, https://github.com/jupyter/jupyter_core/
* **matplotlib**, 3.5.1, PSF-2.0 License, Copyright Matplotlib Development Team, https://github.com/matplotlib/matplotlib
* **netcdf4**, 1.5.8, MIT License, Copyright 2008 Jeffrey Whitaker, https://github.com/Unidata/netcdf4-python
* **numpy**, 1.22.1, BSD 3-Clause License, Copyright 2005-2022 NumPy Developers, https://github.com/numpy/numpy
* **pyorbital**, 1.7.1, GPL-3.0 License, Copyright 2012-2015 and 2018 The Pytroll crew, https://github.com/pytroll/pyorbital
* **pyproj**, 3.2.1, MIT License, Copyright 2019-2022 Open source contributors, https://github.com/pyproj4/pyproj/
* **pyresample**, 1.22.3, LGPL-3.0 License, Copyright 2013-2022 Pyresample developers, https://github.com/pytroll/pyresample
* **pyspectral**, 0.11.0, GPL-3.0 License, Copyright 2013-2021 Pytroll, https://github.com/pytroll/pyspectral
* **python-dateutil**, 2.8.2, Apache-2.0 License, Copyright 2017 Paul Ganssle and dateutil contributors, https://github.com/dateutil/dateutil
* **python-geotiepoints**, 1.4.1, GPL-3.0 License, Copyright 2012-2019 Pytroll Team, https://github.com/pytroll/python-geotiepoints
* **satpy**, 0.33.1, GPL-3.0, Copyright 2009-2022 The PyTroll Team, https://github.com/pytroll/satpy
* **Shapely**, 1.8.0, BSD-3-Clause License, Copyright 2007-2022 Shapely Contributors, https://github.com/shapely/shapely
* **urllib3**, 1.26.8, MIT License, Copyright 2008-2020 Andrey Petrov and contributors, https://github.com/urllib3/urllib3
* **wget**, 3.2, GPL-3.0 License, Copyright 2017 Free Software Foundation, Inc., https://www.gnu.org/software/wget/
* **xarray**, 0.20.0, Apache-2.0 License, Copyright 2014-2019 xarray Developers, https://github.com/pydata/xarray

### Installing

You can **clone this repo** and run the notebooks on your local Jupyter notebook
server. You will have to reproduce the settings. The following
section provides you more information on how to reproduce the set up on your 
local machine.

In case you wish to reproduce the application case on your local setup, 
the following Python version and Python packages will be required:

* Python version: **Python 3.9.12**
* Python packages: see [requirements.txt](./requirements.txt)

Python packages can be installed with:
`pip install -r requirements.txt`. 






